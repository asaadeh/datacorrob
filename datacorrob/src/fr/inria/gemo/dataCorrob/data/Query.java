/**
 * 
 */
package fr.inria.gemo.dataCorrob.data;

/**
 * @author alban
 *
 */
public class Query {
	
	private String id;
	
	private String name;
	
	private String answer;
	
	public Query(String id, String name, String answer) {
		this.id=id;
		this.name=name;
		this.answer=answer;
	}

	public String getId() {
		return id;
	}
	
	public String getName() {
		return name;
	}
	
	public String getAnswer() {
		return answer;
	}
}
