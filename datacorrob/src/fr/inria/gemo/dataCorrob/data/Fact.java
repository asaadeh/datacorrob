/**
 * Fact.java in dataCorrob (C) 25 juil. 08 by agalland
 */
package fr.inria.gemo.dataCorrob.data;

import java.util.Vector;

/**
 * This class implements a fact of the graph.
 * @author agalland (Alban Galland) for Inria Saclay
 */
public class Fact extends Node {
	
	private String queryId;
	
	/**
	 * This field is the internal way to store the truth of the fact
	 */
	private boolean isTrue;

	/**
	 * This function is the standard constructor of the class.
	 * @param id the id of the fact
	 * @param isTrue the truth value of the fact
	 */
	public Fact(int id, boolean isTrue, double perror, double pforgpos, double pforgneg, String queryId) {
		super(id,perror,pforgpos,pforgneg);
		this.isTrue = isTrue;
		this.queryId = queryId;
	}

	/*
	 * (non-Javadoc)
	 * @see fr.inria.gemo.dataCorrob.data.Node#isFact()
	 */
	@Override
	public boolean isFact() {
		return true;
	}

	/*
	 * (non-Javadoc)
	 * @see fr.inria.gemo.dataCorrob.data.Node#isTrue()
	 */
	@Override
	public boolean isTrue() {
		return this.isTrue;
	}

	public String getQueryId() {
		return this.queryId;
	}
	
	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		String s = "Fact " + this.getId() + " : "+this.isTrue()+" "+this.getQueryId()+" (NbPosChildren="
				+ this.getNbPosChildren() + ", NbNegChildren="
				+ this.getNbNegChildren() + ") : ";
		Vector<Link> l = this.getLinks();
		for (int i = 0; i < l.size(); i++) {
			s += l.get(i).getTo() + "(" + l.get(i).getWeight() + ") ";
		}
		return s;
	}
}
