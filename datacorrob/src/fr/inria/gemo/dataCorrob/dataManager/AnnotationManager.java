/**
 * AnnotationManager.java in dataCorrob (C) 29 juil. 08 by agalland
 */
package fr.inria.gemo.dataCorrob.dataManager;

import fr.inria.gemo.dataCorrob.arguments.Arguments;

/**
 * This interface describes the part of a manager which allows to modify the
 * data
 * @author agalland (Alban Galland) for Inria Saclay
 */
public abstract class AnnotationManager {
	/**
	 * This function is the static way to create anotationManager according to the arguments
	 * @param args the arguments of the program
	 * @return a new annotation manager
	 */
	public static AnnotationManager create(Arguments args) {
		return new SimpleAnnotationManager(args);
	}
	/**
	 * This function set an annotation (a couple attribute-value) given by a method
	 * @param id  the id of the node
	 * @param value the value of the annotation
	 * @return the last value associated to this attribute by this method
	 */
	public abstract double setValue(int id,double value); 
	
	/**
	 * This function get the value of an annotation given by a method
	 * @param id  the id of the node
	 * @return the value associated to this attribute by this method
	 */
	public abstract double getValue(int id);
	
	/**
	 * This function add a value to the current annotation given by a method
	 * @param id  the id of the node
	 * @param value the value of the annotation
	 * @return the new value associated with the annotation
	 */
	public abstract double addToValue(int id,double value);
	
	/**
	 * This function reset all the annotation for a given method
	 * @param value the default value to which the annotations must be set
	 */
	public abstract void reset(double value);

}
