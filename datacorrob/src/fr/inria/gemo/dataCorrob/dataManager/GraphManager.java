/**
 * GraphManager.java in dataCorrob (C) 29 juil. 08 by agalland
 */
package fr.inria.gemo.dataCorrob.dataManager;

import java.util.Iterator;

import fr.inria.gemo.dataCorrob.arguments.Arguments;
import fr.inria.gemo.dataCorrob.data.Fact;
import fr.inria.gemo.dataCorrob.data.Link;
import fr.inria.gemo.dataCorrob.data.Source;

/**
 * This interface represents the graph manager part of a datamanager
 * @author agalland (Alban Galland) for INRIA Saclay
 */
public abstract class GraphManager {
	/**
	 * This function is the static way to create GraphManager accordng to the arguments
	 * @param args the arguments of the program
	 * @return a new graph manager
	 */
	public static GraphManager create(Arguments args) {
		return new SimpleGraphManager(args);
	}

	/**
	 * This function adds a fact to the data manager
	 * @param f the fact to be added
	 */
	public abstract void addFact(Fact f);

	/**
	 * This function adds a source to the data manager
	 * @param s the source to be added
	 */
	public abstract void addSource(Source s);

	/**
	 * This function adds a node to the children of a node
	 * @param id the id of the node
	 * @param idchild the id of the child to add
	 * @param weight the weight of the link
	 */
	public abstract void addChildToNode(int id, int idchild, double weight);

	/**
	 * This function gives access to the number of nodes of the graph
	 * @return the number of nodes of the graph
	 */
	public abstract int getNbNodes();

	/**
	 * This function gives access to the list of id of the graph
	 * @return the list of id of the graph
	 */
	public abstract Iterator<Integer> getNodes();

	/**
	 * This function gives access to the number of facts of the graph
	 * @return the number of facts of the graph
	 */
	public abstract int getNbFacts();
	
	public abstract int getNbPosFacts();
	
	public abstract int getNbNegFacts();

	/**
	 * This function gives access to the list of id of facts of the graph
	 * @return the list of id of the graph
	 */
	public abstract Iterator<Integer> getFacts();

	/**
	 * This function gives access to the number of sources of the graph
	 * @return the number of sources of the graph
	 */
	public abstract int getNbSources();

	/**
	 * This function gives access to the list of id of sources of the graph
	 * @return the list of id of the graph
	 */
	public abstract Iterator<Integer> getSources();

	/**
	 * This function gives access to the means of the distribution of the probability of error of the facts
	 * @return mu the means
	 */
	public abstract double getMu();

	/**
	 * This function gives access to the standard deviation of the distribution of the probability of error of the facts
	 * @return sigma the standard deviation
	 */
	public abstract double getSigma();

	/**
	 * This function answers if a node is a fact
	 * @param id the id of the node
	 * @return true if the node is a fact, false else
	 */
	public abstract boolean getIsFact(int id);

	/**
	 * This function answers if a node is a source
	 * @param id the id of the node
	 * @return true if the node is a source, false else
	 */
	public abstract boolean getIsSource(int id);

	public abstract int getNbChildren(int id);
	
	/**
	 * This function gives access to the number of positive children of a node
	 * @param id the id of the node
	 * @return the number of children of the node
	 */
	public abstract int getNbPosChildren(int id);

	/**
	 * This function gives access to the number of negative children of a node
	 * @param id the id of the node
	 * @return the number of children of the node
	 */
	public abstract int getNbNegChildren(int id);

	/**
	 * This function gives access to the probability of error 
	 * @param id the id of the node
	 * @return the probability of error of the node
	 */
	public abstract double getPError(int id);

	public abstract double getPForgPos(int id);
	
	public abstract double getPForgNeg(int id);
	
	/**
	 * This function gives write access to the probability of error
	 * @param id the id of the node
	 * @param e the new value of the probability of error
	 */
	public abstract void setPError(int id,double e);

	public abstract void setPForgPos(int id,double p);
	
	public abstract void setPForgNeg(int id, double p);
	
	/**
	 * This function gives access to the query id linked to the fact 
	 * @param id the id of the node
	 * @return the query id
	 */
	public abstract String getQueryId(int id);

	/**
	 * This function answers is a node (likely a fact) is true
	 * @param id the id of the node
	 * @return the truth of the node
	 */
	public abstract boolean getIsTrue(int id);

	/**
	 * This function gives access to the epsilon parameter of a synthetic node
	 * @param id the id of the node
	 * @return the epsilon parameter
	 */
	public abstract double getEpsilon(int id);

	/**
	 * This function gives access to the phi pos parameter of a synthetic node
	 * @param id the id of the node
	 * @return the phi pos parameter
	 */
	public abstract double getPhiPos(int id);

	/**
	 * This function gives access to the phi neg parameter of a synthetic node
	 * @param id the id of the node
	 * @return the phi neg parameter
	 */
	public abstract double getPhiNeg(int id);

	/**
	 * This function gives access to the links of a node
	 * @param id the id of the node
	 * @return the links of the node
	 */
	public abstract Iterator<Link> getLinks(int id);

	/**
	 * This function gives access to the string describing a node
	 * @param id the if of the node
	 * @return the string describing the node
	 */
	public abstract String toString(int id);
}
