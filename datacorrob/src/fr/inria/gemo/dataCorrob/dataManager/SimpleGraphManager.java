/**
 * SimpleDataManager.java in dataCorrob (C) 25 juil. 08 by agalland
 */
package fr.inria.gemo.dataCorrob.dataManager;

import java.util.Iterator;
import java.util.Vector;

import fr.inria.gemo.dataCorrob.arguments.Arguments;
import fr.inria.gemo.dataCorrob.data.Fact;
import fr.inria.gemo.dataCorrob.data.Link;
import fr.inria.gemo.dataCorrob.data.Node;
import fr.inria.gemo.dataCorrob.data.Source;
import fr.inria.gemo.dataCorrob.data.SyntheticFact;
import fr.inria.gemo.dataCorrob.data.SyntheticSource;

/**
 * This class defines a simple data manager, which enable access to the
 * graph and its annotations.
 * @author agalland (Alban Galland) for Inria Saclay
 */
public class SimpleGraphManager extends GraphManager {
	/**
	 * This field is the internal way to store the nodes
	 */
	private Vector<Node> nodes;
	/**
	 * This field is the internal way to store the list of ids
	 */
	private Vector<Integer> ids;
	/**
	 * This field is the internal way to store the list of ids of facts
	 */
	private Vector<Integer> factids;
	/**
	 * This field is the internal way to store the list of ids of sources
	 */
	private Vector<Integer> sourceids;
	
	private double mu;
	
	private double sigma;
	
	private int nbPosFacts;
	
	private int nbNegFacts;

	/**
	 * This function is the standard constructor of the class
	 */
	protected SimpleGraphManager(Arguments args) {
		this.nodes = new Vector<Node>();
		this.nodes.setSize(args.nbFacts+args.nbSources);
		this.ids = new Vector<Integer>();
		this.factids = new Vector<Integer>();
		this.sourceids = new Vector<Integer>();
		double avgEpsilonFacts = args.FactEpsilon.p*(1-(args.FactMaxEpsilon.size+args.FactMinEpsilon.size))+
		args.FactMaxEpsilon.p*args.FactMaxEpsilon.size+
		args.FactMinEpsilon.p*args.FactMinEpsilon.size;
		double avgEpsilonSources = args.SourceEpsilon.p*(1-(args.SourceMaxEpsilon.size+args.SourceMinEpsilon.size))+
		args.SourceMaxEpsilon.p*args.SourceMaxEpsilon.size+
		args.SourceMinEpsilon.p*args.SourceMinEpsilon.size;
		double avgScore = (1- avgEpsilonFacts*avgEpsilonSources);
		this.mu = (args.rateTrue - (1 - args.rateTrue))*avgScore;
		this.sigma = Math.sqrt((avgScore - mu) * (avgScore - mu) * args.rateTrue + (-avgScore - mu)
				* (-avgScore - mu) * (1 - args.rateTrue));
		this.nbPosFacts=0;
		this.nbNegFacts=0;
	}



	/*
	 * (non-Javadoc)
	 * @see fr.inria.gemo.dataCorrob.dataManager.GraphManager#addFact(int, boolean, double, double, double)
	 */
	@Override
	public void addFact(Fact f) {
		this.ids.add(f.getId());
		this.factids.add(f.getId());
		this.nodes.set(f.getId(), f);
		if(f.isTrue()) {
			nbPosFacts++;
		} else {
			nbNegFacts++;
		}
	}

	/*
	 * (non-Javadoc)
	 * @see fr.inria.gemo.dataCorrob.dataManager.GraphManager#addSource(int, double, double, double)
	 */
	@Override
	public void addSource(Source s) {
		this.ids.add(s.getId());
		this.sourceids.add(s.getId());
		this.nodes.set(s.getId(), s);
	}

	/*
	 * (non-Javadoc)
	 * @see fr.inria.gemo.dataCorrob.dataManager.DataModifier#addChildToNode(int,
	 *      int, double)
	 */
	@Override
	public void addChildToNode(int id, int idchild, double weight) {
		Link l = new Link(id, idchild, weight);
		this.getNodeById(id).addLink(l);
	}

	/*
	 * (non-Javadoc)
	 * @see fr.inria.gemo.dataCorrob.dataManager.DataAccessor#getNbNodes()
	 */
	@Override
	public int getNbNodes() {
		return this.ids.size();
	}

	/*
	 * (non-Javadoc)
	 * @see fr.inria.gemo.dataCorrob.dataManager.DataAccessor#getNodes()
	 */
	@Override
	public Iterator<Integer> getNodes() {
		return this.ids.iterator();
	}

	/*
	 * (non-Javadoc)
	 * @see fr.inria.gemo.dataCorrob.dataManager.GraphManager#getNbFacts()
	 */
	@Override
	public int getNbFacts() {
		return this.factids.size();
	}

	@Override
	public int getNbNegFacts() {
		return this.nbNegFacts;
	}

	@Override
	public int getNbPosFacts() {
		return this.nbPosFacts;
	}
	
	/*
	 * (non-Javadoc)
	 * @see fr.inria.gemo.dataCorrob.dataManager.GraphManager#getFacts()
	 */
	@Override
	public Iterator<Integer> getFacts() {
		return this.factids.iterator();
	}

	/*
	 * (non-Javadoc)
	 * @see fr.inria.gemo.dataCorrob.dataManager.GraphManager#getNbSources()
	 */
	@Override
	public int getNbSources() {
		return this.sourceids.size();
	}

	/*
	 * (non-Javadoc)
	 * @see fr.inria.gemo.dataCorrob.dataManager.GraphManager#getSources()
	 */
	@Override
	public Iterator<Integer> getSources() {
		return this.sourceids.iterator();
	}
	
	@Override
	public double getMu() {
		return this.mu;
	}

	@Override
	public double getSigma() {
		return this.sigma;
	}

	/*
	 * (non-Javadoc)
	 * @see fr.inria.gemo.dataCorrob.dataManager.DataAccessor#getIsFact(int)
	 */
	@Override
	public boolean getIsFact(int id) {
		return this.getNodeById(id).isFact();
	}

	/*
	 * (non-Javadoc)
	 * @see fr.inria.gemo.dataCorrob.dataManager.DataAccessor#getIsSource(int)
	 */
	@Override
	public boolean getIsSource(int id) {
		return this.getNodeById(id).isSource();
	}

	@Override
	public int getNbChildren(int id) {
		return this.getNbPosChildren(id)+this.getNbNegChildren(id);
	}
	
	/*
	 * (non-Javadoc)
	 * @see fr.inria.gemo.dataCorrob.dataManager.DataAccessor#getNbPosChildren(int)
	 */
	@Override
	public int getNbPosChildren(int id) {
		return this.getNodeById(id).getNbPosChildren();
	}

	/*
	 * (non-Javadoc)
	 * @see fr.inria.gemo.dataCorrob.dataManager.DataAccessor#getNbNegChildren(int)
	 */
	@Override
	public int getNbNegChildren(int id) {
		return this.getNodeById(id).getNbNegChildren();
	}


	@Override
	public double getPError(int id) {
		return this.getNodeById(id).getPError();
	}
	

	@Override
	public double getPForgNeg(int id) {
		return this.getNodeById(id).getPForgNeg();
	}



	@Override
	public double getPForgPos(int id) {
		return this.getNodeById(id).getPForgPos();
	}
	
	@Override
	public void setPError(int id, double e) {
		Node n = this.nodes.get(id);
		n.setPError(e);
	}

	@Override
	public void setPForgNeg(int id, double p) {
		Node n = this.nodes.get(id);
		n.setPForgNeg(p);
	}



	@Override
	public void setPForgPos(int id, double p) {
		Node n = this.nodes.get(id);
		n.setPForgPos(p);
	}
	
	/*
	 * (non-Javadoc)
	 * @see fr.inria.gemo.dataCorrob.dataManager.DataAccessor#getIsTrue(int)
	 */
	@Override
	public boolean getIsTrue(int id) {
		return this.getNodeById(id).isTrue();
	}
	
	@Override
	public String getQueryId(int id) {
		Node n = this.nodes.get(id);
		if(Fact.class.isAssignableFrom(n.getClass())) {
			return ((Fact) n).getQueryId();
		} else {
			return ""+id;
		}
	}

	@Override
	public double getEpsilon(int id) {
		Node n = this.getNodeById(id);
		if (n.getClass().equals(SyntheticFact.class)) {
			return ((SyntheticFact) n).getEpsilon();
		} else if (n.getClass().equals(SyntheticSource.class)) {
			return ((SyntheticSource) n).getEpsilon();
		} else {
			return 0;
		}
	}

	/*
	 * (non-Javadoc)
	 * @see fr.inria.gemo.dataCorrob.dataManager.DataAccessor#getphiPos(int)
	 */
	@Override
	public double getPhiPos(int id) {
		Node n = this.getNodeById(id);
		if (n.getClass().equals(SyntheticFact.class)) {
			return ((SyntheticFact) n).getPhiPos();
		} else if (n.getClass().equals(SyntheticSource.class)) {
			return ((SyntheticSource) n).getPhiPos();
		} else {
			return 0;
		}
	}

	/*
	 * (non-Javadoc)
	 * @see fr.inria.gemo.dataCorrob.dataManager.DataAccessor#getphiNeg(int)
	 */
	@Override
	public double getPhiNeg(int id) {
		Node n = this.getNodeById(id);
		if (n.getClass().equals(SyntheticFact.class)) {
			return ((SyntheticFact) n).getPhiNeg();
		} else if (n.getClass().equals(SyntheticSource.class)) {
			return ((SyntheticSource) n).getPhiNeg();
		} else {
			return 0;
		}
	}

	/*
	 * (non-Javadoc)
	 * @see fr.inria.gemo.dataCorrob.dataManager.DataAccessor#getLinks(int)
	 */
	@Override
	public Iterator<Link> getLinks(int id) {
		return this.getNodeById(id).getLinks().iterator();
	}

	/*
	 * (non-Javadoc)
	 * @see fr.inria.gemo.dataCorrob.dataManager.DataAccessor#toString(int)
	 */
	@Override
	public String toString(int id) {
		return this.getNodeById(id).toString();
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		String s = "SimpleDataManager (nbNodes=" + this.getNbNodes() + "): \n";
		Iterator<Integer> nodes = this.getNodes();
		while (nodes.hasNext()) {
			s += this.toString(nodes.next()) + "\n";
		}
		return s;
	}

	/**
	 * This function is the internal way to access to a node
	 * @param id the id of the node
	 * @return the corresponding node
	 */
	private Node getNodeById(int id) {
		return this.nodes.get(new Integer(id));
	}
}
