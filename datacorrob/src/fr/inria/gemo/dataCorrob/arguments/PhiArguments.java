/**
 * ForgetArguments.java in dataCorrob (C) august 08 by agalland
 */
package fr.inria.gemo.dataCorrob.arguments;

import org.w3c.dom.Node;

/**
 * This class implements a sub-part of the arguments used to run the project,
 * describing a sub-distribution of forget probabilities.
 * @author agalland (Alban Galland) for Inria Saclay
 */
public class PhiArguments {
	/**
	 * This field is the average positive forget probability of the
	 * sub-distribution
	 */
	public double pPos;
	/**
	 * This field is the average negative forget probability of the
	 * sub-distribution
	 */
	public double pNeg;
	/**
	 * This field is the delta of the positive forget probability of the
	 * sub-distribution
	 */
	public double dpPos;
	/**
	 * This field is the delta of the negative forget probability of the
	 * sub-distribution
	 */
	public double dpNeg;
	/**
	 * This field is the size of the sub-distribution (the rate on the total
	 * distribution)
	 */
	public double size;

	/**
	 * This function is the standard constructor of the class.
	 * @param pPos the average positive forget probability of the
	 *            sub-distribution
	 * @param pNeg the average negative forget probability of the
	 *            sub-distribution
	 * @param dpPos the delta of the positive forget probability of the
	 *            sub-distribution
	 * @param dpNeg the delta of the negative forget probability of the
	 *            sub-distribution
	 * @param size the size of the sub-distribution (the rate on the total
	 *            distribution)
	 */
	public PhiArguments(double pPos, double pNeg, double dpPos,
			double dpNeg, double size) {
		this.pPos = pPos;
		this.pNeg = pNeg;
		this.dpPos = dpPos;
		this.dpNeg = dpNeg;
		this.size = size;
	}
	
	/**
	 * This function read the forget node type of the file
	 * @param n the forget node
	 * @return the corresponding arguments
	 */
	public static PhiArguments readPhi(Node n) {
		double pPos = new Double(n.getAttributes().getNamedItem("pPos")
				.getNodeValue());
		double dpPos = new Double(n.getAttributes().getNamedItem("dpPos")
				.getNodeValue());
		double pNeg = new Double(n.getAttributes().getNamedItem("pNeg")
				.getNodeValue());
		double dpNeg = new Double(n.getAttributes().getNamedItem("dpNeg")
				.getNodeValue());
		return new PhiArguments(pPos, pNeg, dpPos, dpNeg, 1);
	}

	/**
	 * This function read the forgetM node type of the file
	 * @param n the forget node
	 * @return the corresponding arguments
	 */
	public static PhiArguments readPhiM(Node n) {
		double pPos = new Double(n.getAttributes().getNamedItem("pPos")
				.getNodeValue());
		double dpPos = new Double(n.getAttributes().getNamedItem("dpPos")
				.getNodeValue());
		double pNeg = new Double(n.getAttributes().getNamedItem("pNeg")
				.getNodeValue());
		double dpNeg = new Double(n.getAttributes().getNamedItem("dpNeg")
				.getNodeValue());
		double size = new Double(n.getAttributes().getNamedItem("size")
				.getNodeValue());
		return new PhiArguments(pPos, pNeg, dpPos, dpNeg, size);
	}
}
