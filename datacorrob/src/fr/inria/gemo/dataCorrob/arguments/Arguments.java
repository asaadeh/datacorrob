/**
 * Arguments.java in dataCorrob (C) august 08 by agalland
 */
package fr.inria.gemo.dataCorrob.arguments;

import java.io.File;
import java.io.IOException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import fr.inria.gemo.dataCorrob.graphGenerator.GraphGenerator;
import fr.inria.gemo.dataCorrob.method.Method;
import fr.inria.gemo.dataCorrob.test.Test;

/**
 * This class implements the list of the argument used to run the project. It
 * reads arguments.xml (or any file following datacorrob.xsd schema) and stores
 * it.
 * @author agalland (Alban Galland) for Inria Saclay
 */
public class Arguments {
	/**
	 * This field is the number of iterations to be done by the test to assume
	 * convergence
	 */
	public int nbTotIter;
	/**
	 * This field is the number of steps to be done by the convergence test. The
	 * number of iteration is divided by the number of steps.
	 */
	public int nbSteps;
	/**
	 * This field specifies if the tests must be run several times
	 */
	public boolean multi;
	/**
	 * This field is the number of time the test must be run
	 */
	public int nbRuns;
	/**
	 * This field specifies if the scores must be normalized
	 */
	public boolean norm;
	/**
	 * This field specifies if some dot must be printed on standard output to
	 * follow the iterations
	 */
	public boolean printDots;
	/**
	 * This field specifies if the standard output must be system.out or the file out.txt
	 */
	public boolean printInFile;
	/**
	 * This field specifies if dots must be printed by graph tests
	 */
	public boolean point;
	/**
	 * This field specifies if lines must be printed between dot by graph tests
	 */
	public boolean line;
	/**
	 * This field is the type of test to be run
	 */
	public Class<?> typeTest;
	/**
	 * This field is the type of propagator (method) to be run
	 */
	public Class<?> typeMethod;
	/**
	 * This field is the type of graph generator to be used to create the graph
	 */
	public Class<?> typeGraphGenerator;
	/**
	 * This field specifies if the real data must be updated from the web
	 */
	public boolean update;
	/**
	 * This field specifies if the graph must be completed using functional dependencies
	 */
	public boolean dependencies;
	/**
	 * This field specifies if the score must be post-treated using functional dependencies
	 */
	public boolean postTreatementDependencies;
	/**
	 * This field is the number of facts on the graph
	 */
	public int nbFacts;
	/**
	 * This field is the rate of true facts amon the facts
	 */
	public double rateTrue;
	/**
	 * This field is the number of sources on the graph
	 */
	public int nbSources;
	/**
	 * This field is the precision distribution of the main group of facts
	 */
	public EpsilonArguments FactEpsilon;
	/**
	 * This field is the forget distribution of the main group of facts
	 */
	public PhiArguments FactPhi;
	/**
	 * This field is the precision distribution of the minimum group of facts
	 */
	public EpsilonArguments FactMinEpsilon;
	/**
	 * This field is the forget distribution of the minimum group of facts
	 */
	public PhiArguments FactMinPhi;
	/**
	 * This field is the precision distribution of the maximum group of facts
	 */
	public EpsilonArguments FactMaxEpsilon;
	/**
	 * This field is the forget distribution of the maximum group of facts
	 */
	public PhiArguments FactMaxPhi;
	/**
	 * This field is the precision distribution of the main group of sources
	 */
	public EpsilonArguments SourceEpsilon;
	/**
	 * This field is the forget distribution of the main group of sources
	 */
	public PhiArguments SourcePhi;
	/**
	 * This field is the precision distribution of the minimum group of sources
	 */
	public EpsilonArguments SourceMinEpsilon;
	/**
	 * This field is the forget distribution of the minimum group of sources
	 */
	public PhiArguments SourceMinPhi;
	/**
	 * This field is the precision distribution of the maximum group of sources
	 */
	public EpsilonArguments SourceMaxEpsilon;
	/**
	 * This field is the forget distribution of the maximum group of sources
	 */
	public PhiArguments SourceMaxPhi;

	/**
	 * This function is the standard constructor of the class.
	 * @param file the file which contains the arguments of the program,
	 *            following datacorrob.xsd schema.
	 */
	public Arguments(String file) {
		try {
			DocumentBuilder docbuilder = DocumentBuilderFactory.newInstance()
			.newDocumentBuilder();
			Document doc = docbuilder.parse(new File(file));
			Node n1 = doc.getElementsByTagName("Test").item(0);
			this.typeTest = Test.toClass(n1.getAttributes().getNamedItem(
			"classT").getNodeValue());
			n1 = doc.getElementsByTagName("IterParams").item(0);
			this.nbTotIter = new Integer(n1.getAttributes().getNamedItem(
			"nbTotIter").getNodeValue());
			this.nbSteps = new Integer(n1.getAttributes().getNamedItem(
			"nbSteps").getNodeValue());
			this.multi = new Boolean(n1.getAttributes().getNamedItem(
			"multi").getNodeValue());
			this.nbRuns = new Integer(n1.getAttributes().getNamedItem(
			"nbRuns").getNodeValue());
			this.printInFile = new Boolean(n1.getAttributes().getNamedItem(
			"printInFile").getNodeValue());
			n1 = doc.getElementsByTagName("GraphParams").item(0);
			this.point = new Boolean(n1.getAttributes().getNamedItem("point")
					.getNodeValue());
			this.line = new Boolean(n1.getAttributes().getNamedItem("line")
					.getNodeValue());
			n1 = doc.getElementsByTagName("Method").item(0);
			this.typeMethod = Method.toClass(n1.getAttributes()
					.getNamedItem("classM").getNodeValue());
			n1 = doc.getElementsByTagName("ScoreParams").item(0);
			this.norm = new Boolean(n1.getAttributes().getNamedItem("norm")
					.getNodeValue());
			this.printDots = new Boolean(n1.getAttributes().getNamedItem(
			"printDots").getNodeValue());
			this.postTreatementDependencies = new Boolean(n1.getAttributes().getNamedItem(
			"postTreatementDependencies").getNodeValue());
			n1 = doc.getElementsByTagName("GraphGenerator").item(0);
			this.typeGraphGenerator = GraphGenerator.toClass(n1
					.getAttributes().getNamedItem("classGG").getNodeValue());
			this.update = new Boolean(n1.getAttributes()
					.getNamedItem("update").getNodeValue());
			this.dependencies = new Boolean(n1.getAttributes()
					.getNamedItem("dependencies").getNodeValue());
			n1 = doc.getElementsByTagName("Facts").item(0);
			this.nbFacts = new Integer(n1.getAttributes().getNamedItem("nb")
					.getNodeValue());
			this.rateTrue = new Double(n1.getAttributes().getNamedItem("rate")
					.getNodeValue());
			NodeList nl = n1.getChildNodes();
			for (int i = 0; i < nl.getLength(); i++) {
				if (nl.item(i).getNodeType() == Node.ELEMENT_NODE) {
					if (nl.item(i).getNodeName().equalsIgnoreCase("Epsilon")) {
						this.FactEpsilon = EpsilonArguments.readEpsilon(nl.item(i));
					} else if (nl.item(i).getNodeName().equalsIgnoreCase(
					"EpsilonMin")) {
						this.FactMinEpsilon = EpsilonArguments.readEpsilonM(nl.item(i));
					} else if (nl.item(i).getNodeName().equalsIgnoreCase(
					"EpsilonMax")) {
						this.FactMaxEpsilon = EpsilonArguments.readEpsilonM(nl.item(i));
					} else if (nl.item(i).getNodeName().equalsIgnoreCase(
					"Phi")) {
						this.FactPhi = PhiArguments.readPhi(nl.item(i));
					} else if (nl.item(i).getNodeName().equalsIgnoreCase(
					"PhiMin")) {
						this.FactMinPhi = PhiArguments.readPhiM(nl.item(i));
					} else if (nl.item(i).getNodeName().equalsIgnoreCase(
					"PhiMax")) {
						this.FactMaxPhi = PhiArguments.readPhiM(nl.item(i));
					}
				}
			}
			this.FactEpsilon.size -= (this.FactMinEpsilon.size + this.FactMaxEpsilon.size);
			this.FactPhi.size -= (this.FactMinPhi.size + this.FactMaxPhi.size);
			n1 = doc.getElementsByTagName("Sources").item(0);
			this.nbSources = new Integer(n1.getAttributes().getNamedItem("nb")
					.getNodeValue());
			nl = n1.getChildNodes();
			for (int i = 0; i < nl.getLength(); i++) {
				if (nl.item(i).getNodeType() == Node.ELEMENT_NODE) {
					if (nl.item(i).getNodeName().equalsIgnoreCase("Epsilon")) {
						this.SourceEpsilon = EpsilonArguments.readEpsilon(nl.item(i));
					} else if (nl.item(i).getNodeName().equalsIgnoreCase(
					"EpsilonMin")) {
						this.SourceMinEpsilon = EpsilonArguments.readEpsilonM(nl
								.item(i));
					} else if (nl.item(i).getNodeName().equalsIgnoreCase(
					"EpsilonMax")) {
						this.SourceMaxEpsilon = EpsilonArguments.readEpsilonM(nl
								.item(i));
					} else if (nl.item(i).getNodeName().equalsIgnoreCase(
					"Phi")) {
						this.SourcePhi = PhiArguments.readPhi(nl.item(i));
					} else if (nl.item(i).getNodeName().equalsIgnoreCase(
					"PhiMin")) {
						this.SourceMinPhi = PhiArguments.readPhiM(nl.item(i));
					} else if (nl.item(i).getNodeName().equalsIgnoreCase(
					"PhiMax")) {
						this.SourceMaxPhi = PhiArguments.readPhiM(nl.item(i));
					}
				}
			}
			this.SourceEpsilon.size -= (this.SourceMinEpsilon.size + this.SourceMaxEpsilon.size);
			this.SourcePhi.size -= (this.SourceMinPhi.size + this.SourceMaxPhi.size);
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
