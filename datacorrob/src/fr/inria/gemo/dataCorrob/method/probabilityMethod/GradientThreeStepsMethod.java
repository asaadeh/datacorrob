/**
 * GradientThreeStepsMethod.java in dataCorrob
 * Copyright (C) by INRIA-Saclay, Alban Galland, 17 nov. 2008, All rights reserved
 */
package fr.inria.gemo.dataCorrob.method.probabilityMethod;

import java.util.Iterator;

import fr.inria.gemo.dataCorrob.arguments.Arguments;
import fr.inria.gemo.dataCorrob.data.Link;
import fr.inria.gemo.dataCorrob.dataManager.GraphManager;

/**
 * This class implements 
 */
public class GradientThreeStepsMethod extends ThreeStepsMethod {

	private static double delta = 0.001;

	private static double step = 1;

	/**
	 * This function is the standard constructor of the class.
	 * @param gm
	 * @param args
	 */
	public GradientThreeStepsMethod(GraphManager gm, Arguments args) {
		super(gm, args);
	}

	@Override
	protected void computeAlphaFact(int f) {
		double logPd = 0;
		double logP = 0;
		double af = this.alpha.getValue(f);
		if (af>=1-GradientThreeStepsMethod.delta) {
			af = 1 - 2*GradientThreeStepsMethod.delta;
		}
		double afd = af+GradientThreeStepsMethod.delta;
		double ef = this.epsilon.getValue(f);
		if(ef<=0) {
			ef = GradientThreeStepsMethod.delta;
		} else if (ef>=1) {
			ef = 1 - GradientThreeStepsMethod.delta;
		}
		Iterator <Link> links = this.gm.getLinks(f);
		while(links.hasNext()) {
			Link l = links.next();
			double es = this.epsilon.getValue(l.getTo());
			if(es<=0) {
				es = GradientThreeStepsMethod.delta;
			} else if (es>=1) {
				es = 1 - GradientThreeStepsMethod.delta;
			}
			if(l.isPos()) {
				logP += af*Math.log(es*ef)+(1-af)*Math.log(1-es*ef);
				logPd += afd*Math.log(es*ef)+(1-afd)*Math.log(1-es*ef);
			} else if (l.isNeg()) {
				logP += af*Math.log(1-es*ef)+(1-af)*Math.log(es*ef);
				logPd += afd*Math.log(1-es*ef)+(1-afd)*Math.log(es*ef);
			}
		}
		double alp;
		if(logPd==logP) {
			alp=af;
		} else {
			alp = af-logP/(logPd-logP)*GradientThreeStepsMethod.delta*GradientThreeStepsMethod.step;
		}
		this.alpha.setValue(f, alp);
	}

	@Override
	protected void computeEpsilonFact(int f) {
		double logPd = 0;
		double logP = 0;
		double af = this.alpha.getValue(f);
		double ef = this.epsilon.getValue(f);
		if(ef<=0) {
			ef = GradientThreeStepsMethod.delta;
		} else if (ef>=1-GradientThreeStepsMethod.delta) {
			ef = 1 - 2*GradientThreeStepsMethod.delta;
		}
		double efd = ef+GradientThreeStepsMethod.delta;
		Iterator <Link> links = this.gm.getLinks(f);
		while(links.hasNext()) {
			Link l = links.next();
			double es = this.epsilon.getValue(l.getTo());
			if(es<=0) {
				es = GradientThreeStepsMethod.delta;
			} else if (es>=1) {
				es = 1 - GradientThreeStepsMethod.delta;
			}
			if(l.isPos()) {
				logP += af*Math.log(es*ef)+(1-af)*Math.log(1-es*ef);
				logPd += af*Math.log(es*efd)+(1-af)*Math.log(1-es*efd);
			} else if (l.isNeg()) {
				logP += af*Math.log(1-es*ef)+(1-af)*Math.log(es*ef);
				logPd += af*Math.log(1-es*efd)+(1-af)*Math.log(es*efd);
			}
		}
		double eps;
		if(logPd==logP) {
			eps = ef;
		} else {
			eps  = ef-logP/(logPd-logP)*GradientThreeStepsMethod.delta*GradientThreeStepsMethod.step;
		}
		this.epsilon.setValue(f, eps);
	}

	@Override
	protected void computeEpsilonSource(int s) {
		double logPd = 0;
		double logP = 0;
		double es = this.epsilon.getValue(s);
		if(es<=0) {
			es = GradientThreeStepsMethod.delta;
		} else if (es>=1-GradientThreeStepsMethod.delta) {
			es = 1 - 2*GradientThreeStepsMethod.delta;
		}
		double esd = es+GradientThreeStepsMethod.delta;
		Iterator <Link> links = this.gm.getLinks(s);
		while(links.hasNext()) {
			Link l = links.next();
			double af = this.alpha.getValue(l.getTo());
			double ef = this.epsilon.getValue(l.getTo());
			if(ef<=0) {
				ef = GradientThreeStepsMethod.delta;
			} else if (ef>=1) {
				ef = 1 - GradientThreeStepsMethod.delta;
			}
			if(l.isPos()) {
				logP += af*Math.log(es*ef)+(1-af)*Math.log(1-es*ef);
				logPd += af*Math.log(esd*ef)+(1-af)*Math.log(1-esd*ef);
			} else if (l.isNeg()) {
				logP += af*Math.log(1-es*ef)+(1-af)*Math.log(es*ef);
				logPd += af*Math.log(1-esd*ef)+(1-af)*Math.log(esd*ef);
			}
		}
		double eps;
		if(logPd==logP) {
			eps = es;
		} else {
			eps = es-logP/(logPd-logP)*GradientThreeStepsMethod.delta*GradientThreeStepsMethod.step;
		}
		this.epsilon.setValue(s, eps);
	}
}
