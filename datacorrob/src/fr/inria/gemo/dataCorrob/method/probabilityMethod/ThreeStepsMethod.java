/**
 * ThreeStepsMethod.java in dataCorrob (C) 6 oct. 08 by agalland 
 */
package fr.inria.gemo.dataCorrob.method.probabilityMethod;

import java.util.Iterator;

import fr.inria.gemo.dataCorrob.arguments.Arguments;
import fr.inria.gemo.dataCorrob.dataManager.GraphManager;

/**
 * This class implements 
 * @author agalland (Alban Galland) for INRIA Saclay
 */
public abstract class ThreeStepsMethod extends TwoStepsMethod {

	/**
	 * This function is the standard constructor of the class.
	 * @param gm
	 */
	protected ThreeStepsMethod(GraphManager gm,Arguments args) {
		super(gm,args);
	}

	abstract protected void computeEpsilonFact(int f);

	/* (non-Javadoc)
	 * @see fr.inria.gemo.dataCorrob.method.Method#getScore(int)
	 */
	@Override
	public double getSimpleScore(int id) {
		if(this.gm.getIsFact(id)) {
			if (this.alpha.getValue(id)>0.5) {
				return 1 - this.epsilon.getValue(id);
			} else if (this.alpha.getValue(id)<0.5){
				return - (1 -this.epsilon.getValue(id));
			} else {
				return 0;
			}
		}else{
			return (1 - this.epsilon.getValue(id));
		}
	}

	/* (non-Javadoc)
	 * @see fr.inria.gemo.dataCorrob.method.Method#init()
	 */
	@Override
	public void init() {
		this.nbIter=0;
		this.normFactor = 1;
		this.alpha.reset(0.5);
		this.epsilon.reset(0.1);
	}

	/* (non-Javadoc)
	 * @see fr.inria.gemo.dataCorrob.method.Method#iterate()
	 */
	@Override
	protected void iterate() {
		this.iterateAlphaFacts();
		this.iterateEpsilonFacts();
		this.iterateEpsilonSources();
		this.nbIter++;
		if(this.nbIter<this.args.nbTotIter/2) {
			this.normFactor = 1-(2*this.nbIter/((double) this.args.nbTotIter));
		} else {
			this.normFactor=0;
		}
	}

	protected void iterateEpsilonFacts() {
		Iterator <Integer> ids = this.gm.getFacts();
		while(ids.hasNext()) {
			this.computeEpsilonFact(ids.next());
		}
		this.normalizeEpsilonFacts();
	}

	protected void normalizeEpsilonFacts(){
		double maxEpsilon=0;
		double minEpsilon=1;
		Iterator <Integer> ids = this.gm.getFacts();
		while(ids.hasNext()) {
			Integer id = ids.next();
			double ef = this.epsilon.getValue(id); 
			if(ef>maxEpsilon) {
				maxEpsilon = ef;
			}
			if(ef<minEpsilon) {
				minEpsilon=ef;
			}
		}
		ids = this.gm.getFacts();
		while(ids.hasNext()) {
			Integer id = ids.next();
			double value = this.epsilon.getValue(id);
			double normedValue1;
			if(maxEpsilon!=minEpsilon) {
				normedValue1 = (value-minEpsilon)/(maxEpsilon-minEpsilon);
			} else {
				normedValue1 = value;
			}
			double normedValue2;
			if (maxEpsilon-minEpsilon>1) {
				normedValue2 = normedValue1;
			} else if (minEpsilon<0) {
				normedValue2 = normedValue1;
			} else {
				normedValue2 = value+(1-maxEpsilon);
				//another possibilitynormedValue2 = value/maxEpsilon;
			}
			double mergedValue = this.normFactor * normedValue1 + (1-this.normFactor) * normedValue2;
			this.epsilon.setValue(id, mergedValue);
		}
	}
}
