/**
 * ECMTwoStepsMethod.java in dataCorrob
 * Copyright (C) by INRIA-Saclay, Alban Galland, 18 nov. 2008, All rights reserved
 */
package fr.inria.gemo.dataCorrob.method.probabilityMethod;

import java.util.Iterator;

import fr.inria.gemo.dataCorrob.arguments.Arguments;
import fr.inria.gemo.dataCorrob.data.Link;
import fr.inria.gemo.dataCorrob.dataManager.GraphManager;

/**
 * This class implements 
 */
public class ECMTwoStepsMethod extends TwoStepsMethod {

	/**
	 * This function is the standard constructor of the class.
	 * @param gm
	 * @param args
	 */
	public ECMTwoStepsMethod(GraphManager gm, Arguments args) {
		super(gm, args);
	}

	@Override
	protected void computeAlphaFact(int f) {
		double product1 = 1;
		double product2 = 1;
		Iterator <Link> links = this.gm.getLinks(f);
		while(links.hasNext()) {
			Link l = links.next();
			double es = this.epsilon.getValue(l.getTo());
			if(l.isPos()) {
				product1  *= es;
				product2 *= (1-es);
			} else if (l.isNeg()) {
				product1 *= (1-es);
				product2 *= es;
			}
		}
		if (product1<product2) {
			this.alpha.setValue(f, 0);
		}else if (product1>product2){
			this.alpha.setValue(f, 1);		
		} else {
			this.alpha.setValue(f, 0.5);
		}
	}

	protected void computeEpsilonSource(int s) {
		double sum1 = 0;
		double sum2 = 0;
		Iterator <Link> links = this.gm.getLinks(s);
		while(links.hasNext()) {
			Link l = links.next();
			double af = this.alpha.getValue(l.getTo());
			if(l.isPos()) {
				sum1 += af;
				sum2 += (2*af-1);
			} else if (l.isNeg()) {
				sum1 += (1-af);
				sum2 += (1-2*af);
			}
		}
		this.epsilon.setValue(s, sum1/sum2);
	}
	
}
