/**
 * NormalizedSourcesOPICPropagator.java in dataCorrob (C) 29 juil. 08 by
 * agalland
 */
package fr.inria.gemo.dataCorrob.method.genericMethod;

import java.util.Iterator;

import fr.inria.gemo.dataCorrob.arguments.Arguments;
import fr.inria.gemo.dataCorrob.data.Link;
import fr.inria.gemo.dataCorrob.dataManager.GraphManager;

/**
 * This class implements a propagator which normalize the money of a source
 * according to his number of children and doesn't take neagtive in account
 * @author agalland (Alban Galland) for Inria Saclay
 */
public class NormalizedSourcesMethod extends SumGenericMethod {
	/**
	 * Maximum number of sources positively linked to a fact
	 */
	private double maxNbPosSources;
	/**
	 * Average number of sources positively linked to a fact
	 */
	private double avgNbPosSources;
	/**
	 * Average number of facts positively linked to a source
	 */
	private double avgNbPosFacts;

	/**
	 * This function is the standard constructor for the graph
	 * @param dm the data manager for the propagation
	 */
	public NormalizedSourcesMethod(GraphManager gm,Arguments args) {
		super(gm,args);
		this.maxNbPosSources=0;
		this.avgNbPosSources=0;
		this.avgNbPosFacts=0;
		double sumPosSources=0;
		Iterator <Integer> ids = this.gm.getFacts();
		while(ids.hasNext()) {
			Integer id = ids.next();
			Iterator <Link> ls = this.gm.getLinks(id);
			double nbPosSources=0;
			while(ls.hasNext()) {
				Link l =ls.next();
				if(l.isPos()) {
					nbPosSources++;
					sumPosSources++;
				}
			}
			if (nbPosSources>this.maxNbPosSources) {
				this.maxNbPosSources = nbPosSources;
			}
		}
		this.avgNbPosSources=sumPosSources/gm.getNbFacts();
		double sumPosFacts=0;
		ids = this.gm.getSources();
		while(ids.hasNext()) {
			Integer id = ids.next();
			Iterator <Link> ls = this.gm.getLinks(id);
			while(ls.hasNext()) {
				Link l =ls.next();
				if(l.isPos()) {
					sumPosFacts++;
				}
			}
		}
		this.avgNbPosFacts=sumPosFacts/gm.getNbSources();
	}

	/*
	 * (non-Javadoc)
	 * @see fr.inria.gemo.dataCorrob.method.genericMethod.GenericMethod#initFact(int)
	 */
	@Override
	protected double initFact(int id) {
		return this.gm.getNbPosChildren(id)
		/ this.avgNbPosSources;
	}

	/*
	 * (non-Javadoc)
	 * @see fr.inria.gemo.dataCorrob.method.genericMethod.GenericMethod#initSource(int)
	 */
	@Override
	protected double initSource(int id) {
		return this.gm.getNbPosChildren(id)
		/ this.avgNbPosFacts;
	}

	/*
	 * (non-Javadoc)
	 * @see fr.inria.gemo.dataCorrob.method.genericMethod.GenericMethod#scoreFact(int)
	 */
	@Override
	protected double scoreFact(int id) {
		double sums = 0;
		Iterator<Link> links = this.gm.getLinks(id);
		while (links.hasNext()) {
			Link l = links.next();
			if (l.isPos()) {
				sums += this.score.getValue(l.getTo());
			}
		}
		return sums / this.maxNbPosSources;
	}

	/*
	 * (non-Javadoc)
	 * @see fr.inria.gemo.dataCorrob.method.genericMethod.GenericMethod#scoreSource(int)
	 */
	@Override
	protected double scoreSource(int id) {
		double sumf = 0;
		Iterator<Link> links = this.gm.getLinks(id);
		while (links.hasNext()) {
			Link l = links.next();
			if (l.isPos()) {
				sumf += this.score.getValue(l.getTo());
			}
		}
		return sumf / this.gm.getNbPosChildren(id);
	}

	/*
	 * (non-Javadoc)
	 * @see fr.inria.gemo.dataCorrob.method.Method#getScore(int)
	 */
	@Override
	public double getSimpleScore(int id) {
		if(this.gm.getIsFact(id)) {
			return 2*this.score.getValue(id)-1;
		} else {
			return this.score.getValue(id);
		}
	}
}
