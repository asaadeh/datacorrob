/**
 * CosineAbsPropagator.java in dataCorrob (C) august 08 by agalland
 */
package fr.inria.gemo.dataCorrob.method.genericMethod;

import fr.inria.gemo.dataCorrob.arguments.Arguments;
import fr.inria.gemo.dataCorrob.dataManager.GraphManager;

/**
 * This class implements a cosine propagator with abs weights
 * @author agalland (Alban Galland) for Inria Saclay
 */
public class CosineAbsMethod extends CosineMethod {
	/**
	 * This function is the standard constructor of the class.
	 * @param dm the data manager
	 */
	public CosineAbsMethod(GraphManager gm,Arguments args) {
		super(gm,args);
	}

	/*
	 * (non-Javadoc)
	 * @see fr.inria.gemo.dataCorrob.propagator.CosinePropagator#computeCoeff(double)
	 */
	@Override
	protected double computeCoeff(double posCash) {
		return Math.abs(posCash);
	}
}
