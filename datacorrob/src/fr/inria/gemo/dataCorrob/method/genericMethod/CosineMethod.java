/**
 * CosinePropagator.java in dataCorrob (C) august 08 by agalland
 */
package fr.inria.gemo.dataCorrob.method.genericMethod;

import java.util.Iterator;

import fr.inria.gemo.dataCorrob.arguments.Arguments;
import fr.inria.gemo.dataCorrob.data.Link;
import fr.inria.gemo.dataCorrob.dataManager.GraphManager;

/**
 * This abstract class implements the cosine propagator
 * @author agalland (Alban Galland) for Inria Saclay
 */
public abstract class CosineMethod extends SumGenericMethod {
	/**
	 * This function is the standard constructor of the class.
	 * @param dm the data manager
	 */
	protected CosineMethod(GraphManager gm,Arguments args) {
		super(gm,args);
	}

	/**
	 * This function computes a coefficient for a given score
	 * @param posCash the score to compute the coefficient
	 * @return the coefficient
	 */
	abstract protected double computeCoeff(double posCash);

	/*
	 * (non-Javadoc)
	 * @see fr.inria.gemo.dataCorrob.method.genericMethod.GenericMethod#initFact(int)
	 */
	@Override
	protected double initFact(int id) {
		double suml = 0;
		double nl = 0;
		Iterator<Link> links = this.gm.getLinks(id);
		while (links.hasNext()) {
			suml += links.next().getWeight();
			nl++;
		}
		if (nl > 0) {
			return  suml / nl;
		} else {
			return 0;
		}
	}

	/*
	 * (non-Javadoc)
	 * @see fr.inria.gemo.dataCorrob.method.genericMethod.GenericMethod#initSource(int)
	 */
	@Override
	protected double initSource(int id) {
		return 1.0;
	}

	/*
	 * (non-Javadoc)
	 * @see fr.inria.gemo.dataCorrob.method.genericMethod.GenericMethod#scoreFact(int)
	 */
	@Override
	protected double scoreFact(int id) {
		double sumsl = 0;
		double sums = 0;
		Iterator<Link> links = this.gm.getLinks(id);
		while (links.hasNext()) {
			Link l = links.next();
			double posCash = this.score.getValue(l.getTo());
			sumsl += l.getWeight() * posCash * this.computeCoeff(posCash);
			if (l.getWeight() != 0) {
				sums += this.computeCoeff(posCash);
			}
		}
		return sumsl / sums;
	}

	/*
	 * (non-Javadoc)
	 * @see fr.inria.gemo.dataCorrob.method.genericMethod.GenericMethod#scoreSource(int)
	 */
	@Override
	protected double scoreSource(int id) {
		double sumlf = 0;
		double suml = 0;
		double sumf = 0;
		Iterator<Link> links = this.gm.getLinks(id);
		while (links.hasNext()) {
			Link l = links.next();
			double posCash = this.score.getValue(l.getTo());
			sumlf += l.getWeight() * posCash;
			suml += l.getWeight() * l.getWeight();
			sumf += posCash * posCash;
		}
		return sumlf / (Math.sqrt(suml) * Math.sqrt(sumf));
	}
}
