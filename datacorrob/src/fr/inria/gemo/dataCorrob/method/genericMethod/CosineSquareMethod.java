/**
 * CosineSquarePropagator.java in dataCorrob (C) august 08 by agalland
 */
package fr.inria.gemo.dataCorrob.method.genericMethod;

import fr.inria.gemo.dataCorrob.arguments.Arguments;
import fr.inria.gemo.dataCorrob.dataManager.GraphManager;

/**
 * This class implements a propagator based on the cosine method with square
 * weights
 * @author agalland (Alban Galland) for Inria Saclay
 */
public class CosineSquareMethod extends CosineMethod {
	/**
	 * This function is the standard constructor of the class.
	 * @param dm the data manager
	 */
	public CosineSquareMethod(GraphManager gm,Arguments args) {
		super(gm,args);
	}

	/*
	 * (non-Javadoc)
	 * @see fr.inria.gemo.dataCorrob.propagator.CosinePropagator#computeCoeff(double)
	 */
	@Override
	protected double computeCoeff(double posCash) {
		return posCash * posCash;
	}
}
