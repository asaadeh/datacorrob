/**
 * PosNegOPICPropagator.java in dataCorrob (C) 30 juil. 08 by agalland
 */
package fr.inria.gemo.dataCorrob.method.opicMethod;

import java.util.Iterator;

import fr.inria.gemo.dataCorrob.arguments.Arguments;
import fr.inria.gemo.dataCorrob.data.Link;
import fr.inria.gemo.dataCorrob.dataManager.AnnotationManager;
import fr.inria.gemo.dataCorrob.dataManager.GraphManager;
import fr.inria.gemo.dataCorrob.method.Method;

/**
 * This class implements a modified positive-negative OPIC propagator
 * @author agalland (Alban Galland) for Inria Saclay
 */
public class PosNegOPICMethod extends Method {
	/**
	 * This field is the maximum money of the virtual node before to empty it
	 */
	private static double maxMoneyVirtual = 10;
	/**
	 * This field is the cash of the virtual node
	 */
	private double cashVirtual;
	/**
	 * This field is the internal way to store the positive cash of the nodes
	 */
	private AnnotationManager posCash;
	/**
	 * This field is the internal way to store the positive bank of the nodes
	 */
	private AnnotationManager posBank;
	/**
	 * This field is the internal way to store the positive cash of the nodes
	 */
	private AnnotationManager negCash;
	/**
	 * This field is the internal way to store the positive bank of the nodes
	 */
	private AnnotationManager negBank;

	/**
	 * This function is the standard constructor of the class
	 * @param dm the data manager for the propagation
	 */
	public PosNegOPICMethod(GraphManager gm,Arguments args) {
		super(gm,args);
		this.cashVirtual=0;
		this.posCash =  AnnotationManager.create(args);
		this.posBank = AnnotationManager.create(args);
		this.negCash =  AnnotationManager.create(args);
		this.negBank = AnnotationManager.create(args);
	}

	/*
	 * (non-Javadoc)
	 * @see fr.inria.gemo.dataCorrob.method.Method#getScore(int)
	 */
	@Override
	public double getSimpleScore(int id) {
		double score = (this.posCash.getValue(id) + this.posBank.getValue(id))
		/ (this.posCash.getValue(id) + this.posBank.getValue(id)
		+ this.negCash.getValue(id) + this.negBank.getValue(id));
		if (this.gm.getIsFact(id)) {
			return 2*score-1;
		} else {
			return score;
		}
	}

	/*
	 * (non-Javadoc)
	 * @see fr.inria.gemo.dataCorrob.method.Method#init()
	 */
	@Override
	public void init() {
		this.posCash.reset(0);
		this.posBank.reset(0);
		this.negCash.reset(0);
		this.negBank.reset(0);
		Iterator<Integer> nodes = this.gm.getNodes();
		while (nodes.hasNext()) {
			Integer id = nodes.next();
			double nbPosChildren = this.gm.getNbPosChildren(id);
			double nbNegChildren = this.gm.getNbNegChildren(id);
			double nbChildren = nbPosChildren + nbNegChildren;
			if (nbChildren > 0) {
				double posCash = nbPosChildren / (nbChildren);
				double negCash = nbNegChildren / (nbChildren);
				this.posCash.setValue(id,posCash);
				this.negCash.setValue(id,negCash);
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * @see fr.inria.gemo.dataCorrob.method.Method#iterate()
	 */
	@Override
	protected void iterate() {
		Iterator <Integer> ids = this.gm.getNodes();
		while(ids.hasNext()) {
			Integer id = ids.next();
			this.manageVirtualCash();
			double posCash = this.posCash.getValue(id);
			double posBank = this.posBank.addToValue(id, posCash);
			double negCash = this.negCash.getValue(id);
			double negBank = this.negBank.addToValue(id, negCash);
			
			double totBank = posBank + negBank;
			double totCash = posCash + negCash;
			if (totBank > 0) {
				double posProb = posBank / totBank;
				double posTransfer = posProb * posProb * totCash;
				double negTransfer = (1 - posProb) * (1 - posProb) * totCash;
				int nbPosChildren = this.gm.getNbPosChildren(id);
				if (nbPosChildren == 0) {
					this.cashVirtual += posTransfer;
				}
				int nbNegChildren = this.gm.getNbNegChildren(id);
				if (nbNegChildren == 0) {
					this.cashVirtual += negTransfer;
				}
				Iterator<Link> links = this.gm.getLinks(id);
				while (links.hasNext()) {
					Link l = links.next();
					if (l.isPos()) {
						this.posCash.addToValue(l.getTo(),posTransfer / nbPosChildren);
					} else if (l.isNeg()) {
						this.negCash.addToValue(l.getTo(),negTransfer / nbNegChildren);
					}
				}
				this.cashVirtual += totCash - (posTransfer + negTransfer);
			}
		}
	}

	/**
	 * This private function checks the virtual cash and distributes it to all the nodes
	 */
	private void manageVirtualCash() {
		if (this.cashVirtual > PosNegOPICMethod.maxMoneyVirtual) {
			double money = this.cashVirtual / (2* this.gm.getNbNodes());
			Iterator<Integer> nodes = this.gm.getNodes();
			while (nodes.hasNext()) {
				int id = nodes.next();
				this.posCash.addToValue(id, money);
				this.negCash.addToValue(id, money);
			}
			this.cashVirtual = 0;
		}
	}
}
