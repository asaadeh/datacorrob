/**
 * Method.java in dataCorrob (C) 24 sept. 08 by agalland 
 */
package fr.inria.gemo.dataCorrob.method;

import java.util.Iterator;

import fr.inria.gemo.dataCorrob.arguments.Arguments;
import fr.inria.gemo.dataCorrob.dataManager.GraphManager;
import fr.inria.gemo.dataCorrob.method.baseLineMethod.BaseLinePosMethod;
import fr.inria.gemo.dataCorrob.method.baseLineMethod.BaseLinePosNegMethod;
import fr.inria.gemo.dataCorrob.method.baseLineMethod.BaseLineTruthFinderMethod;
import fr.inria.gemo.dataCorrob.method.genericMethod.CosineAbsMethod;
import fr.inria.gemo.dataCorrob.method.genericMethod.CosineSquareMethod;
import fr.inria.gemo.dataCorrob.method.genericMethod.NormalizedSourcesMethod;
import fr.inria.gemo.dataCorrob.method.genericMethod.ProductMethod;
import fr.inria.gemo.dataCorrob.method.opicMethod.OPICMethod;
import fr.inria.gemo.dataCorrob.method.opicMethod.PosNegOPICMethod;
import fr.inria.gemo.dataCorrob.method.probabilityMethod.ApproxThreeStepsMethod;
import fr.inria.gemo.dataCorrob.method.probabilityMethod.ApproxTwoStepsMethod;
import fr.inria.gemo.dataCorrob.method.probabilityMethod.ECMTwoStepsMethod;
import fr.inria.gemo.dataCorrob.method.probabilityMethod.GradientThreeStepsMethod;
import fr.inria.gemo.dataCorrob.method.probabilityMethod.GradientTwoStepsMethod;

/**
 * This abstract class implements a generic method
 * @author agalland (Alban Galland) for INRIA Saclay
 */
public abstract class Method {
	/**
	 * This field is the internal link to the data manager
	 */
	protected GraphManager gm;
	/**
	 * This field is the internal way to store the average of the distribution
	 * of scores
	 */
	private double muScore;
	/**
	 * This field is the internal way to store the sigma of the distribution of
	 * scores
	 */
	private double sigmaScore;
	/**
	 * This field is the internal way to store the arguments
	 */
	protected Arguments args;

	/**
	 * This function is the standard constructor of the class.
	 * @param gm the data manager
	 */
	protected Method(GraphManager gm,Arguments args) {
		this.gm = gm;
		this.muScore = 0;
		this.sigmaScore = 0;
		this.args = args;
	}

	/**
	 * This function computes the score of a node
	 * Score of a fact is between -1 and 1 (sign means true/false, (|value|+1)/2 means confidence, 0 means no opinion), score of a source is between 0 and 1 (value means expertise)
	 * @param id the id of the node
	 * @return the score of the node
	 */
	abstract protected double getSimpleScore(int id);

	/**
	 * This function initialize the value for the computation on the graph
	 */
	abstract public void init();

	/**
	 * This function iterate one time the computation on the graph
	 */
	abstract protected void iterate();

	/**
	 * This function iterates the method on the graph
	 * @param nbIter the number of iteration
	 */
	public void iterate(int nbIter) {
		if (this.args.printDots) {
			System.out.print("Method "+Method.toString(this.getClass())+" : iteration");
		}
		for (int i = 0; i < nbIter; i++) {
			this.iterate();
			if (this.args.printDots) {
				System.out.print(".");
			}
		}
		if (this.args.printDots) {
			System.out.println();
		}
		this.computeMoments();
	}



	/**
	 * This function computes the normalized score of a node
	 * @param id the id of the node
	 * @return the score of the node
	 */
	public double getScore(int id) {
		if(gm.getIsFact(id)) {
			if(this.args.postTreatementDependencies) {
				String query = this.gm.getQueryId(id);
				double maxscore = this.getSimpleScore(id);
				boolean existless=false;
				Iterator <Integer> facts = this.gm.getFacts();
				while(facts.hasNext()) {
					int f = facts.next();
					if(this.gm.getQueryId(f).equals(query)) {
						double score = this.getSimpleScore(f);
						if(score>maxscore) {
							maxscore = this.getSimpleScore(f);
						} else if(score<maxscore)  {
							existless=true;
						}
					}
				}
				double score = this.getSimpleScore(id);
				if(maxscore>score) {
					return Math.min(-0.01, score);
				} else if (existless) {
					return Math.max(0.01,score);
				}else {
					return score;
				}
			} else if (this.args.norm) {
				return ((this.getSimpleScore(id) - this.muScore) * this.gm.getSigma()
						/ this.sigmaScore + this.gm.getMu());
			} else {
				return this.getSimpleScore(id);	
			}
		} else {
			return this.getSimpleScore(id);	
		}
	}

	/**
	 * This function computes an estimation of the truth based on the score
	 * @param id the id of the node
	 * @return the estimation of the truth
	 */
	public boolean getTruth(int id) {
		if(this.gm.getIsFact(id)) {
			return (this.getScore(id) > 0);
		} else {
			return false;
		}
	}

	/**
	 * This function compute the average and the sigma of the distribution of
	 * scores
	 */
	private void computeMoments() {
		double nbScores = 0;
		double sum = 0;
		Iterator<Integer> nodes = this.gm.getNodes();
		while (nodes.hasNext()) {
			int id = nodes.next();
			if (this.gm.getIsFact(id)) {
				nbScores++;
				sum += this.getSimpleScore(id);
			}
		}
		this.muScore = sum / nbScores;
		sum = 0;
		nodes = this.gm.getNodes();
		while (nodes.hasNext()) {
			int id = nodes.next();
			if (this.gm.getIsFact(id)) {
				sum += (this.getSimpleScore(id) - this.muScore)
				* (this.getSimpleScore(id) - this.muScore);
			}
		}
		this.sigmaScore = Math.sqrt(sum / nbScores);
	}

	/**
	 * This static method transforms a class on a string
	 * @param type the method sub-class 
	 * @return the corresponding string
	 */
	public static String toString(Class<?> type) {
		String typeMethod = null;
		if (AllMethods.class.equals(type)) {
			typeMethod="All";
		} else if (BaseLinePosMethod.class.equals(type)) {
			typeMethod = "BaseLinePos";
		} else if (BaseLinePosNegMethod.class.equals(type)) {
			typeMethod = "BaseLinePosNeg";
		} else if (OPICMethod.class.equals(type)) {
			typeMethod = "OPIC";
		} else if (NormalizedSourcesMethod.class.equals(type)) {
			typeMethod = "NormalizedSources";
		} else if (PosNegOPICMethod.class.equals(type)) {
			typeMethod = "PosNegOPIC";
		} else if (CosineAbsMethod.class.equals(type)) {
			typeMethod = "CosineAbs";
		} else if (CosineSquareMethod.class.equals(type)) {
			typeMethod = "CosineSquare";
		} else if (ProductMethod.class.equals(type)) {
			typeMethod = "Product";
		} else if (ApproxThreeStepsMethod.class.equals(type)) {
			typeMethod = "ApproxThreeSteps";
		}  else if (ApproxTwoStepsMethod.class.equals(type)) {
			typeMethod = "ApproxTwoSteps";
		}  else if (GradientThreeStepsMethod.class.equals(type)) {
			typeMethod = "GradientThreeSteps";
		}  else if (GradientTwoStepsMethod.class.equals(type)) {
			typeMethod = "GradientTwoSteps";
		}  else if (ECMTwoStepsMethod.class.equals(type)) {
			typeMethod = "ECMTwoSteps";
		}  else if (BaseLineTruthFinderMethod.class.equals(type)) {
			typeMethod = "BaseLineTruthFinder";
		}  else {
			System.err.println("Error (Method.java) : unknown Method type");
		}
		return typeMethod;
	}

	/**
	 * This static method transforms a string in a class
	 * @param type the string designing a method sub-class
	 * @return the corresponding class
	 */
	public static Class<?> toClass(String type) {
		Class<?> typeMethod = null;
		if (type.equalsIgnoreCase("All")) {
			typeMethod = AllMethods.class;
		} else if (type.equalsIgnoreCase("BaseLinePos")) {
			typeMethod = BaseLinePosMethod.class;
		} else if (type.equalsIgnoreCase("BaseLinePosNeg")) {
			typeMethod = BaseLinePosNegMethod.class;
		} else if (type.equalsIgnoreCase("OPIC")) {
			typeMethod = OPICMethod.class;
		} else if (type.equalsIgnoreCase("NormalizedSources")) {
			typeMethod = NormalizedSourcesMethod.class;
		} else if (type.equalsIgnoreCase("PosNegOPIC")) {
			typeMethod = PosNegOPICMethod.class;
		} else if (type.equalsIgnoreCase("CosineAbs")) {
			typeMethod = CosineAbsMethod.class;
		} else if (type.equalsIgnoreCase("CosineSquare")) {
			typeMethod = CosineSquareMethod.class;
		} else if (type.equalsIgnoreCase("Product")) {
			typeMethod = ProductMethod.class;
		} else if (type.equalsIgnoreCase("ApproxThreeSteps")) {
			typeMethod = ApproxThreeStepsMethod.class;
		}  else if (type.equalsIgnoreCase("ApproxTwoSteps")) {
			typeMethod = ApproxTwoStepsMethod.class;
		}  else if (type.equalsIgnoreCase("GradientThreeSteps")) {
			typeMethod = GradientThreeStepsMethod.class;
		}  else if (type.equalsIgnoreCase("GradientTwoSteps")) {
			typeMethod = GradientTwoStepsMethod.class;
		}  else if (type.equalsIgnoreCase("ECMTwoSteps")) {
			typeMethod = ECMTwoStepsMethod.class;
		}   else if (type.equalsIgnoreCase("BaseLineTruthFinder")) {
			typeMethod = BaseLineTruthFinderMethod.class;
		}  else {
			System.err.println("Error (Method.java) : "+type+" is an unknown Method type");
		}
		return typeMethod;
	}

	/**
	 * This static method transforms a class on a string
	 * @param type the method sub-class 
	 * @return the corresponding string
	 */
	public static Method create(Class<?> type, Arguments args, GraphManager gm) {
		Method m= null;
		if (AllMethods.class.equals(type)) {
			m = new AllMethods(gm,args);
		} else if (BaseLinePosMethod.class.equals(type)) {
			m = new BaseLinePosMethod(gm,args);
		} else if (BaseLinePosNegMethod.class.equals(type)) {
			m = new BaseLinePosNegMethod(gm,args);
		} else if (OPICMethod.class.equals(type)) {
			m = new OPICMethod(gm,args);
		} else if (NormalizedSourcesMethod.class.equals(type)) {
			m = new NormalizedSourcesMethod(gm,args);
		} else if (PosNegOPICMethod.class.equals(type)) {
			m = new PosNegOPICMethod(gm,args);
		} else if (CosineAbsMethod.class.equals(type)) {
			m = new CosineAbsMethod(gm,args);
		} else if (CosineSquareMethod.class.equals(type)) {
			m = new CosineSquareMethod(gm,args);
		} else if (ProductMethod.class.equals(type)) {
			m = new ProductMethod(gm,args);
		} else if (ApproxThreeStepsMethod.class.equals(type)) {
			m = new ApproxThreeStepsMethod(gm,args);
		}  else if (ApproxTwoStepsMethod.class.equals(type)) {
			m = new ApproxTwoStepsMethod(gm,args);
		}  else if (GradientThreeStepsMethod.class.equals(type)) {
			m = new GradientThreeStepsMethod(gm,args);
		}  else if (GradientTwoStepsMethod.class.equals(type)) {
			m = new GradientTwoStepsMethod(gm,args);
		}  else if (ECMTwoStepsMethod.class.equals(type)) {
			m = new ECMTwoStepsMethod(gm,args);
		}  else if (BaseLineTruthFinderMethod.class.equals(type)) {
			m = new BaseLineTruthFinderMethod(gm,args);
		}  else {
			System.err.println("Error (Method.java) : unknown Method type");
		}
		return m;
	}
}
