/**
 * PrintTest.java in dataCorrob (C) september 29 2008 by agalland
 */
package fr.inria.gemo.dataCorrob.test.printTest;

import java.util.Iterator;

import fr.inria.gemo.dataCorrob.arguments.Arguments;
import fr.inria.gemo.dataCorrob.dataManager.GraphManager;
import fr.inria.gemo.dataCorrob.graphGenerator.GraphGenerator;
import fr.inria.gemo.dataCorrob.method.AllMethods;
import fr.inria.gemo.dataCorrob.method.Method;
import fr.inria.gemo.dataCorrob.test.Test;

/**
 * This abstract class defines the tests which could be printed
 * @author agalland (Alban Galland) for Inria Saclay
 */
public abstract class PrintTest  extends Test{
	
	/**
	 * This function is the standard constructor of the class.
	 * @param args the arguments of the program
	 */
	protected PrintTest(Arguments args){
		super(args);
	}

	/**
	 * This function compute a string which is the result of the test
	 * @param gm the data manager
	 * @param m the method used in the test
	 * @return the corresponding string
	 */
	protected abstract String print(GraphManager gm,Method m);

	/**
	 * This function compute the result of the test and saves it
	 * @param m the method used for the test
	 */
	protected abstract void save(GraphManager gm, Method m);

	/**
	 * This function prints the results of a multiple test
	 */
	protected abstract void printMulti();

	@Override
	protected void runTest(GraphManager gm) {
		Method m = Method.create(this.args.typeMethod, this.args, gm);
		m.init();
		m.iterate(this.args.nbTotIter);
		if(m.getClass().equals(AllMethods.class)) {
			System.out.println(PrintTest.toString(this.getClass())+" : run all the methods");
			Iterator <Method> ms = ((AllMethods) m).getMethods();
			while(ms.hasNext()) {
				Method m1 = ms.next();
				this.out.println(" - with method "+Method.toString(m1.getClass()));
				this.out.println(this.print(gm,m1));
			}
		} else {
			this.out.println(PrintTest.toString(this.getClass())+" with method "+Method.toString(m.getClass()));
			this.out.println(this.print(gm,m));
		}
	}

	/*
	 * (non-Javadoc)
	 * @see fr.inria.gemo.dataCorrob.test.Test#runMultiTest()
	 */
	@Override
	protected void runMultiTest() {
		for(int i=0;i<args.nbRuns;i++) {
			GraphManager gm = GraphGenerator.create(args).constructGraph();
			Method m = Method.create(this.args.typeMethod, this.args, gm);
			m.init();
			m.iterate(this.args.nbTotIter);
			this.out.println(PrintTest.toString(this.getClass())+" : Saving...");
			if(m.getClass().equals(AllMethods.class)) {
				Iterator <Method> ms = ((AllMethods) m).getMethods();
				while(ms.hasNext()) {
					this.save(gm,ms.next());
				}
			} else {
				this.save(gm,m);
			}
		}
		this.printMulti();
	}
}
