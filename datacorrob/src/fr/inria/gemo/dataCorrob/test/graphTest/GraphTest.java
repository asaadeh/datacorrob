/**
 * GraphTest.java in dataCorrob (C) august 08 by agalland
 */
package fr.inria.gemo.dataCorrob.test.graphTest;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.util.Iterator;

import fr.inria.gemo.dataCorrob.arguments.Arguments;
import fr.inria.gemo.dataCorrob.dataManager.GraphManager;
import fr.inria.gemo.dataCorrob.graphGenerator.GraphGenerator;
import fr.inria.gemo.dataCorrob.method.AllMethods;
import fr.inria.gemo.dataCorrob.method.Method;
import fr.inria.gemo.dataCorrob.test.Test;
import fr.inria.gemo.dataCorrob.test.printTest.PrintTest;

/**
 * This abstract test defines the test which use some plotting function
 * @author agalland (Alban Galland) for Inria Saclay
 */
public abstract class GraphTest extends Test {
	
	/**
	 * The title of the graph
	 */
	protected String title;
	
	private PrintStream out;
	
	protected int sizePoint;
	
	protected int sizeFont;
	
	protected int sizeLine;
	
	protected int sizeDash;

	/**
	 * This static field is the color of propagators
	 */
	public static String [] colors = {"blue","cyan","green","red","magenta","orange","yellow","grey"};
	
	/**
	 * This function is the standard constructor of the class.
	 * @param args the arguments of the program
	 * @param title the title of the graph
	 */
	public GraphTest(Arguments args, String title) {
		super(args);
		this.title = title;
		this.sizeFont = 20;
		this.sizePoint = 2;
		this.sizeLine = 2;
		this.sizeDash = 5;
		this.createPlot();
	}
	
	/**
	 * This function give access to the name of the file where to store the output
	 * @return the name of the file
	 */
	protected abstract String getFile();
	
	/**
	 * This function plot the box on which the graph is plotted
	 */
	protected abstract void printBox();

	/**
	 * This function plot a graph for a given method
	 * @param dm the data manager to use
	 * @param m the method used
	 * @param color the color to use
	 */
	protected abstract void plot(GraphManager gm,Method m, String color);
	
	@Override
	protected void runTest(GraphManager gm) {
		Method m = Method.create(this.args.typeMethod, this.args, gm);
		m.init();
		m.iterate(this.args.nbTotIter);
		if(m.getClass().equals(AllMethods.class)) {
			System.out.println(PrintTest.toString(this.getClass())+" : run all the methods");
			Iterator <Method> ms = ((AllMethods) m).getMethods();
			int k=0;
			while(ms.hasNext()) {
				Method subm = ms.next();
				System.out.println(Method.toString(subm.getClass())+" : "+GraphTest.colors[k]);
				this.plot(gm,subm,GraphTest.colors[k]);
				k++;
			}
		} else {
			System.out.println(PrintTest.toString(this.getClass())+" with method "+Method.toString(m.getClass()));
			this.plot(gm,m,"blue");
		}
		this.finishPlot();
	}
	
	/*
	 * (non-Javadoc)
	 * @see fr.inria.gemo.dataCorrob.test.Test#runMultiTest()
	 */
	@Override
    protected void runMultiTest() {
    	System.err.println("Warning(GraphTest) : multi runs is not supported for graph test");
    	GraphManager gm = GraphGenerator.create(this.args).constructGraph();
    	this.runTest(gm);
    }
	
	/**
	 * This function is the internal way to create a stream to plot the graph
	 * @return the stream where to plot the graph
	 */
	protected void createPlot() {
		try {
			this.out = new PrintStream(new FileOutputStream(this.getFile(), false));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			this.out = System.out;
		}
		this.printBox();
	}

	/**
	 * This function plot a point in the graph
	 * @param out the stream to plot the graph
	 * @param x the x coordinate of the point
	 * @param y the y coordinate of the point
	 * @param color the color of the point
	 */
	protected void plotPoint(double x, double y, String color) {
		this.out.println("<circle cx='" + (x* 1000) + "' cy='"+ -(y*1000) +"' r='"+this.sizePoint+"' stroke='"+color+
				"' fill='"+color+"'/>");
	}

	/**
	 * This function plot a line in the graph
	 * @param out the stream to plot the graph
	 * @param x1 the x coordinate of the first point of the line
	 * @param x2 the x coordinate of the second point of the line
	 * @param y1 the y coordinate of the first point of the line
	 * @param y2 the y coordinate of the second point of the line
	 * @param color the color of the point
	 */
	protected void plotLine(double x1, double x2, double y1,
			double y2, String color) {
		this.out.println("<line x1='" + (x1 * 1000) + "' y1='" + (-(y1 * 1000))
				+ "' x2='" + (x2 * 1000) + "' y2='" + (-(y2 * 1000))
				+ "' style='stroke:" + color + ";stroke-width:"+this.sizeLine+"'/>");
	}
	
	protected void plotDashLine(double x1, double x2, double y1,
			double y2) {
		this.out.println("<line x1='" + (x1 * 1000) + "' y1='" + (-(y1 * 1000))
				+ "' x2='" + (x2 * 1000) + "' y2='" + (-(y2 * 1000))
				+ "' style='stroke:black;stroke-dasharray:"+this.sizeDash+","+this.sizeDash+"'/>");
	}
	
	protected void plotVerticalSide(double x, double y1,double y2) {
		this.plotLine(x, x, y1, y2, "black");
		for (double y = y1; y <= y2; y+=0.1) {
			this.plotLine(x-0.005, x+0.005, y, y, "black");
		}
	}
	
	protected void plotHorizontalSide(double x1, double x2,double y) {
		this.plotLine(x1, x2, y, y, "black");
		for (double x = x1; x <= x2;x+=0.1) {
			this.plotLine(x, x, y-0.005, y+0.005, "black");
		}
	}
	
	protected void plotText(double x, double y, String text) {
		this.out
		.println("<text x='"+(x*1000)+"' y='"+(-(y*1000))+"' font-size='"+this.sizeFont+"' style='text-anchor:middle'>"+text+"</text>");
		
	}
	
	protected void BeginPlot(double x1, double y1, double x2, double y2) {
		this.out
		.println("<svg xmlns='http://www.w3.org/2000/svg' viewBox='"+(x1*1000-100)+" "+(-(y2*1000+100))+" "+((x2-x1)*1000+200)+" "+((y2-y1)*1000+200)+"'>\n<title>"
				+ this.title + "</title>");
	}

	/**
	 * This function close the stream where the graph is printed
	 * @param out the stream where to plot the graph
	 */
	protected void finishPlot() {
		out.println("</svg>");
		out.close();
		System.out.println(this.getFile()+" has been updated...");
	}
}
