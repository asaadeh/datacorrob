/**
 * PlotEstimatorsSourcesTest.java in dataCorrob (C) 1 oct. 08 by agalland 
 */
package fr.inria.gemo.dataCorrob.test.graphTest;

import java.util.Iterator;

import fr.inria.gemo.dataCorrob.arguments.Arguments;
import fr.inria.gemo.dataCorrob.dataManager.GraphManager;
import fr.inria.gemo.dataCorrob.method.Method;

/**
 * This class implements 
 * @author agalland (Alban Galland) for INRIA Saclay
 */
public class PlotEstimatorsSourcesTest extends GraphTest {

	/**
	 * This function is the standard constructor of the class.
	 * @param args
	 */
	public PlotEstimatorsSourcesTest(Arguments args) {
		super(args,"Plot of the estimators of the sources");
		this.sizePoint=1;
	}
	@Override
	protected void plot(GraphManager gm, Method m, String color) {
		Iterator<Integer> nodes = gm.getSources();
		while (nodes.hasNext()) {
			int id = nodes.next();
				double score =m.getScore(id);
				double target = 1-(gm.getPError(id));
				this.plotPoint(target, score, color);
		}
	}
	
	/*
	 * (non-Javadoc)
	 * @see fr.inria.gemo.dataCorrob.test.GraphTest#printBox(java.io.PrintStream)
	 */
	@Override
	protected void printBox() {
		this.BeginPlot(0, 0, 1, 1);
		this.plotVerticalSide(0,0,1);
		this.plotVerticalSide(1,0,1);
		this.plotHorizontalSide(0,1,0);
		this.plotHorizontalSide(0,1,1);
		this.plotDashLine(0.5, 0.5, 0, 1);
		this.plotDashLine(0,1,0.5,0.5);
		this.plotText(-0.01, -0.02, "0");
		this.plotText(1.01, -0.02, "Probability of believing the truth");
		this.plotText(-0.01, 1.01, "score");
		this.plotText(1.01, 1.01, "1");
	}
	
	@Override
	protected String getFile() {
		return "out/EstimatorsSources.xml";
	}
}
