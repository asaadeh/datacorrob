/**
 * PlotDistributionTest.java in dataCorrob (C) august 08 by agalland
 */
package fr.inria.gemo.dataCorrob.test.graphTest;

import java.util.Iterator;

import fr.inria.gemo.dataCorrob.arguments.Arguments;
import fr.inria.gemo.dataCorrob.dataManager.GraphManager;
import fr.inria.gemo.dataCorrob.method.Method;

/**
 * This class implements a test which plots the distributions of the estimators
 * given by the propagator
 * @author agalland (Alban Galland) for Inria Saclay
 */
public class PlotDistributionTest extends GraphTest {
	/**
	 * This field is the step to use for the distribution computation
	 */
	static double step = 0.05;
	/**
	 * This field is the minimum value of the distribution
	 */
	static double min = -1.2;
	/**
	 * This field is the maximum value of the distribution
	 */
	static double max = 1.2;

	/**
	 * This function is the standard constructor of the class.
	 * @param args the arguments of the program
	 * @param mu the average to normalize the score
	 * @param sigma the sigma to normalize the score
	 */
	public PlotDistributionTest(Arguments args) {
		super(args,"Plot of the distributions");
	}

	/*
	 * (non-Javadoc)
	 * @see fr.inria.gemo.dataCorrob.test.graphTest.GraphTest#plot(fr.inria.gemo.dataCorrob.dataManager.DataManager, fr.inria.gemo.dataCorrob.method.Method, java.lang.String, java.io.PrintStream)
	 */
	@Override
	protected void plot(GraphManager gm, Method m, String color) {
		int nbSteps = (int) Math
		.rint((PlotDistributionTest.max - PlotDistributionTest.min)
				/ PlotDistributionTest.step) + 1;
		int[] dist = new int[nbSteps];
		int[] distTh = new int[nbSteps];
		for (int i = 0; i < nbSteps; i++) {
			dist[i] = 0;
			distTh[i] = 0;
		}
		Iterator<Integer> nodes = gm.getFacts();
		while (nodes.hasNext()) {
			int id = nodes.next();
			double score = m.getScore(id);
			dist[this.toIndex(score)]++;
			double scoreTh=0;
			if (gm.getIsTrue(id)) {
				scoreTh = 1-gm.getPError(id);
			} else {
				scoreTh = gm.getPError(id)-1;
			}
			distTh[this.toIndex(scoreTh)]++;
		}
		double nbFacts = gm.getNbFacts();
		if (this.args.point) {
			for (int i = 0; i < nbSteps; i++) {
				double x = i * PlotDistributionTest.step
				+ PlotDistributionTest.min;
				this.plotPoint(x, dist[i] / nbFacts, color);
				this.plotPoint(x, distTh[i] / nbFacts, "black");
			}
		}
		if (this.args.line) {
			for (int i = 0; i < nbSteps - 1; i++) {
				double x = i * PlotDistributionTest.step
				+ PlotDistributionTest.min;
				this.plotLine(x, x + PlotDistributionTest.step, dist[i]
				                                                          / nbFacts, dist[i + 1] / nbFacts, color);
				this.plotLine(x, x + PlotDistributionTest.step, distTh[i]
				                                                            / nbFacts, distTh[i + 1] / nbFacts, "black");
			}
		}
	}

	private int toIndex(double d) {
		return (int) Math.rint((Math.min(PlotDistributionTest.max, Math.max(
				PlotDistributionTest.min,d)) - PlotDistributionTest.min)
				/ PlotDistributionTest.step);
	}
	
	/*
	 * (non-Javadoc)
	 * @see fr.inria.gemo.dataCorrob.test.GraphTest#printBox(java.io.PrintStream)
	 */
	@Override
	protected void printBox() {
		this.BeginPlot(-1, 0, 1, 1);
		this.plotVerticalSide(-1,0,1);
		this.plotVerticalSide(0,0,1);
		this.plotVerticalSide(1,0,1);
		this.plotHorizontalSide(-1,1,0);
		this.plotHorizontalSide(-1,1,1);
		this.plotText(-0.01, -0.02, "0");
		this.plotText(1.01, -0.02, "value");
		this.plotText(-0.01, 1.01, "%");
		this.plotText(1.01, 1.01, "1");
		this.plotText(-1.01, 1.01, "-1");
	}
	
	
	@Override
	protected String getFile() {
		return "out/Distributions.xml";
	}
}
