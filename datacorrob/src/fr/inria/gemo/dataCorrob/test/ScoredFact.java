/**
 * ScoredFact.java in dataCorrob (C) august 08 by agalland
 */
package fr.inria.gemo.dataCorrob.test;

/**
 * This class implements a fact with a score
 * @author agalland (Alban Galland) for Inria Saclay
 */
public class ScoredFact implements Comparable<ScoredFact> {
	/**
	 * This field is the id of the fact
	 */
	public int id;
	/**
	 * This field is the score associated with the fact
	 */
	public double weight;

	/**
	 * This function is the standard constructor of the class.
	 * @param id the id of the fact
	 * @param weight the score associated with the fact
	 */
	public ScoredFact(int id, double weight) {
		this.id = id;
		this.weight = weight;
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	//@Override
	public int compareTo(ScoredFact f2) {
		return 0 - (Double.compare(this.weight, f2.weight));
	}
}
