/**
 * Test.java in dataCorrob (C) august 08 by agalland
 */
package fr.inria.gemo.dataCorrob.test;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintStream;

import fr.inria.gemo.dataCorrob.arguments.Arguments;
import fr.inria.gemo.dataCorrob.dataManager.GraphManager;
import fr.inria.gemo.dataCorrob.graphGenerator.GraphGenerator;
import fr.inria.gemo.dataCorrob.test.graphTest.PlotDistributionTest;
import fr.inria.gemo.dataCorrob.test.graphTest.PlotEstimatorsSourcesTest;
import fr.inria.gemo.dataCorrob.test.graphTest.PlotEstimatorsTest;
import fr.inria.gemo.dataCorrob.test.graphTest.PlotGraphGenerationTest;
import fr.inria.gemo.dataCorrob.test.graphTest.PlotPrecisionRecallTest;
import fr.inria.gemo.dataCorrob.test.printTest.ConvergenceTest;
import fr.inria.gemo.dataCorrob.test.printTest.ErrorTest;
import fr.inria.gemo.dataCorrob.test.printTest.PrecisionRecallTest;
import fr.inria.gemo.dataCorrob.test.printTest.SimpleTest;

/**
 * This abstract class defines the tests
 * @author agalland (Alban Galland) for Inria Saclay
 */
public abstract class Test {
	/**
	 * This field is the arguments of the program
	 */
	protected Arguments args;


	protected PrintStream out;

	/**
	 * This function is the standard constructor of the class.
	 * @param args the arguments of the program
	 */
	public Test(Arguments args) {
		this.args = args;
		if(args.printInFile) {
			try {
				this.out = new PrintStream(new FileOutputStream("out/out.txt", false));
			} catch (FileNotFoundException e) {
				this.out = System.out;
				e.printStackTrace();
			}
		} else {
			this.out = System.out;
		}
	}

	public void run() {
		if(this.args.multi) {
			this.runMultiTest();
		} else {
			GraphManager gm = GraphGenerator.create(this.args).constructGraph();
			this.runTest(gm);
		}
	}

	/**
	 * This function generates a graph and runs a test 
	 */
	protected abstract void runTest(GraphManager gm);

	/**
	 * This function generates graphs, runs test on it and aggregate the results
	 */
	protected abstract void runMultiTest();

	/**
	 * This function transform a string describing the type of test on the
	 * corresponding class
	 * @param type the string describing the type
	 * @return the corresponding class
	 */
	public static Class<?> toClass(String type) {
		Class<?> typeTest = null;
		if (type.equalsIgnoreCase("Simple")) {
			typeTest = SimpleTest.class;
		} else if (type.equalsIgnoreCase("Convergence")) {
			typeTest = ConvergenceTest.class;
		} else if (type.equalsIgnoreCase("Error")) {
			typeTest = ErrorTest.class;
		} else if (type.equalsIgnoreCase("PlotEstimators")) {
			typeTest = PlotEstimatorsTest.class;
		}	else if (type.equalsIgnoreCase("PlotEstimatorsSources")) {
			typeTest = PlotEstimatorsSourcesTest.class;
		} else if (type.equalsIgnoreCase("PrecisionRecall")) {
			typeTest = PrecisionRecallTest.class;
		} else if (type.equalsIgnoreCase("PlotPrecisionRecall")) {
			typeTest = PlotPrecisionRecallTest.class;
		} else if (type.equalsIgnoreCase("PlotDistribution")) {
			typeTest = PlotDistributionTest.class;
		} else if (type.equalsIgnoreCase("PlotGraphGeneration")) {
			typeTest = PlotGraphGenerationTest.class;
		} else if (type.equalsIgnoreCase("All")) {
			typeTest = AllTests.class;
		} else {
			System.err.println("Error (Test.java) : "+type+" is an unknown Test type");
		}
		return typeTest;
	}

	/**
	 * This function transform a sub-class of test on the
	 * corresponding string
	 * @param type the class of the test
	 * @return the corresponding string
	 */
	public static String toString(Class<?> type) {
		String typeTest = null;
		if (SimpleTest.class.equals(type)) {
			typeTest = "Simple";
		} else if (ConvergenceTest.class.equals(type)) {
			typeTest = "Convergence";
		} else if (ErrorTest.class.equals(type)) {
			typeTest = "Error";
		} else if (PlotEstimatorsTest.class.equals(type)) {
			typeTest = "PlotEstimators";
		} else if (PlotEstimatorsSourcesTest.class.equals(type)) {
			typeTest = "PlotEstimatorsSources";
		} else if (PrecisionRecallTest.class.equals(type)) {
			typeTest = "PrecisionRecall";
		} else if (PlotPrecisionRecallTest.class.equals(type)) {
			typeTest = "PlotPrecisionRecall";
		} else if (PlotDistributionTest.class.equals(type)) {
			typeTest = "PlotDistribution";
		} else if (PlotGraphGenerationTest.class.equals(type)) {
			typeTest = "PlotGraphGeneration";
		} else if (AllTests.class.equals(type)) {
			typeTest = "All";
		} else {
			System.err.println("Error (Test.java) : unknown Test type");
		}
		return typeTest;
	}

	/**
	 * This function create a test according to the arguments provided
	 * @param args the arguments of the program
	 * @return
	 */
	public static Test create(Class<?> typeTest,Arguments args) {
		Test t = null;
		if (SimpleTest.class.equals(typeTest)) {
			t = new SimpleTest(args);
		} else if (ConvergenceTest.class.equals(typeTest)) {
			t = new ConvergenceTest(args);
		} else if (ErrorTest.class.equals(typeTest)) {
			t = new ErrorTest(args);
		} else if (PlotEstimatorsTest.class.equals(typeTest)) {
			t = new PlotEstimatorsTest(args);
		} else if (PlotEstimatorsSourcesTest.class.equals(typeTest)) {
			t = new PlotEstimatorsSourcesTest(args);
		} else if (PrecisionRecallTest.class.equals(typeTest)) {
			t = new PrecisionRecallTest(args);
		} else if (PlotPrecisionRecallTest.class.equals(typeTest)) {
			t = new PlotPrecisionRecallTest(args);
		} else if (PlotDistributionTest.class.equals(typeTest)) {
			t = new PlotDistributionTest(args);
		} else if (PlotGraphGenerationTest.class.equals(typeTest)) {
			t = new PlotGraphGenerationTest(args);
		} else if (AllTests.class.equals(typeTest)) {
			t = new AllTests(args);
		} else {
			System.err.println("Error (Test.java) : unknown Test type");
		}
		return t;
	}
}
