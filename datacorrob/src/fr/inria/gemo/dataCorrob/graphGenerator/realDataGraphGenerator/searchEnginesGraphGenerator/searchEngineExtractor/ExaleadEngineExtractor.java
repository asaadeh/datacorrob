/**
 * ExaleadEngineExtractor.java in dataCorrob
 * Copyright (C) by INRIA-Saclay, Alban Galland, 29 oct. 2008, All rights reserved
 */
package fr.inria.gemo.dataCorrob.graphGenerator.realDataGraphGenerator.searchEnginesGraphGenerator.searchEngineExtractor;

/**
 * This class implements 
 */
public class ExaleadEngineExtractor extends HtmlEngineExtractor {

	/* (non-Javadoc)
	 * @see fr.inria.gemo.dataCorrob.graphGenerator.realDataGraphGenerator.searchEnginesGraphGenerator.searchEngineExtractor.HtmlEngineExtractor#getUrl(java.lang.String)
	 */
	@Override
	public String getUrl(String q) {
		return "http://www.exalead.fr/search/results?q="+q.replace(" ", "+");
	}

	@Override
	public String filtering(String s) {
		String fs = s.replaceAll("<span class=\"c87\">[^<]*</span>","");
		fs = fs.replaceAll("&copy; 2000-2008 exalead", "");
		return fs;
	}

}
