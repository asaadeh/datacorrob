/**
 * SearchEnginesGraphGenerator.java in dataCorrob
 * Copyright (C) by INRIA-Saclay, Alban Galland, 13 oct. 2008, All rights reserved
 */
package fr.inria.gemo.dataCorrob.graphGenerator.realDataGraphGenerator.searchEnginesGraphGenerator;

import java.util.Iterator;
import java.util.Vector;

import fr.inria.gemo.dataCorrob.arguments.Arguments;
import fr.inria.gemo.dataCorrob.dataManager.GraphManager;
import fr.inria.gemo.dataCorrob.graphGenerator.realDataGraphGenerator.RealDataGraphGenerator;

/**
 * This class implements 
 */
public class SearchEnginesGraphGenerator extends RealDataGraphGenerator {

	public SearchEnginesGraphGenerator(Arguments args) {
		super(args);
	}

	@Override
	public String getFile() {
		return SearchEngineManager.file;
	}
	
	@Override
	public Iterator<String> getSourceNames() {
		Vector <String> names = new Vector <String>();
		for(int i=0;i<SearchEngineManager.methods.length;i++) {
			names.add(SearchEngineManager.toString(SearchEngineManager.methods[i]));
		}
		return names.iterator();
	}

	/* (non-Javadoc)
	 * @see fr.inria.gemo.dataCorrob.graphGenerator.GraphGenerator#constructGraph()
	 */
	@Override
	public GraphManager constructGraph() {
		if (this.args.update) {
			SearchEngineManager.update();
		}
		return super.constructGraph();
	}
}
