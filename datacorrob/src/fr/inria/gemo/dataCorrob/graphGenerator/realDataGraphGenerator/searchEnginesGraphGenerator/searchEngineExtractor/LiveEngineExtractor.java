/**
 * LiveEngineExtractor.java in dataCorrob
 * Copyright (C) by INRIA-Saclay, Alban Galland, 29 oct. 2008, All rights reserved
 */
package fr.inria.gemo.dataCorrob.graphGenerator.realDataGraphGenerator.searchEnginesGraphGenerator.searchEngineExtractor;

/**
 * This class implements 
 */
public class LiveEngineExtractor extends HtmlEngineExtractor {

	/* (non-Javadoc)
	 * @see fr.inria.gemo.dataCorrob.graphGenerator.realDataGraphGenerator.searchEnginesGraphGenerator.searchEngineExtractor.HtmlEngineExtractor#getUrl(java.lang.String)
	 */
	@Override
	public String getUrl(String q) {
		return "http://search.live.com/results.aspx?q="+q.replace(" ", "+");
	}

	@Override
	public String filtering(String s) {
		String fs = s.replaceAll("&#169; 2008 Microsoft","");
		return fs;
	}

}
