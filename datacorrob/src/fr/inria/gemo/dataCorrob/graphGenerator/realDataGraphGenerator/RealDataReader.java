/**
 * RealDataReader.java in dataCorrob
 * Copyright (C) by INRIA-Saclay, Alban Galland, 4 nov. 2008, All rights reserved
 */
package fr.inria.gemo.dataCorrob.graphGenerator.realDataGraphGenerator;

import java.io.File;
import java.io.IOException;
import java.util.Hashtable;
import java.util.Vector;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import fr.inria.gemo.dataCorrob.data.RealFact;
import fr.inria.gemo.dataCorrob.graphGenerator.realDataGraphGenerator.searchEnginesGraphGenerator.SearchEngineManager;

/**
 * This class implements 
 */
public class RealDataReader {

	String file;
	Document doc;
	Hashtable <String,String> queries;
	Hashtable <String,String> answers;

	public RealDataReader(String file) {
		this.file =file;
		this.queries = new Hashtable <String,String>();
		this.answers = new Hashtable <String,String>();
		try {
			this.doc = this.openFile();
			Node nQueries = this.doc.getElementsByTagName("QueryData").item(0);
			NodeList nl = nQueries.getChildNodes();
			for(int i=0; i<nl.getLength();i++) {
				Node n = nl.item(i);
				if (n.getNodeType() == Node.ELEMENT_NODE) {
					if (n.getNodeName().equalsIgnoreCase("Query")) {
						String queryid = n.getAttributes().getNamedItem(
						"id").getNodeValue();
						Node qn = n.getAttributes().getNamedItem(
						"name");
						String query=null;
						if(qn==null) {
							query=queryid;
						} else {
							query=qn.getNodeValue();
						}
						String answer = n.getAttributes().getNamedItem(
						"answer").getNodeValue();
						queries.put(queryid, query);
						answers.put(queryid, answer);

					}
				}
			}
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public Vector <RealFact> readFile(String source) {
		Vector <RealFact> facts = new Vector <RealFact>();
		Node nEngine = this.getEngineNode(this.doc,source);
		NodeList nl = nEngine.getChildNodes();
		for(int i=0;i<nl.getLength();i++) {
			Node n = nl.item(i);
			if (n.getNodeType() == Node.ELEMENT_NODE) {
				if (n.getNodeName().equalsIgnoreCase("EngineAnswer")) {
					String queryid = n.getAttributes().getNamedItem(
					"ref").getNodeValue();
					String answer = n.getAttributes().getNamedItem(
					"answer").getNodeValue();
					double weight =1.0;
					Node wn = n.getAttributes().getNamedItem("weight");
					if(wn!=null) {
						weight = new Double(wn.getNodeValue());
					}
					if(queries.containsKey(queryid)) {
						facts.add(new RealFact(-1,queryid,queries.get(queryid),answer,answer.equals(answers.get(queryid)),weight));
					}
				}
			}
		}
		return facts;
	}

	private Document openFile() throws ParserConfigurationException, SAXException, IOException {
		DocumentBuilder docbuilder = DocumentBuilderFactory.newInstance()
		.newDocumentBuilder();
		File f = new File(this.file);
		return docbuilder.parse(f);
	}

	private Node getEngineNode(Document doc,String engine) {
		Node nEngine = null;
		NodeList nl = doc.getElementsByTagName("Engine");
		for(int i=0;i<nl.getLength();i++) {
			Node n = nl.item(i);
			String id = n.getAttributes().getNamedItem("id").getNodeValue();
			if(id.equals(engine)) {
				nEngine = n;
			}
		}
		if(nEngine==null) {
			Element newNode = doc.createElement("Engine");
			newNode.setAttribute("id",SearchEngineManager.toString(this.getClass()));
			doc.getElementsByTagName("EngineData").item(0).appendChild(newNode);
			nEngine = newNode;
		}
		return nEngine;
	}

}
