/**
 * BaiduEngineExtractor.java in dataCorrob
 * Copyright (C) by INRIA-Saclay, Alban Galland, 29 oct. 2008, All rights reserved
 */
package fr.inria.gemo.dataCorrob.graphGenerator.realDataGraphGenerator.searchEnginesGraphGenerator.searchEngineExtractor;

/**
 * This class implements 
 */
public class BaiduEngineExtractor extends HtmlEngineExtractor {

	/* (non-Javadoc)
	 * @see fr.inria.gemo.dataCorrob.graphGenerator.realDataGraphGenerator.searchEnginesGraphGenerator.searchEngineExtractor.HtmlEngineExtractor#getUrl(java.lang.String)
	 */
	@Override
	public String getUrl(String q) {
		return "http://www.baidu.com/s?wd="+q.replace(" ", "+");
	}

	@Override
	public String filtering(String s) {
		String fs = s.replaceAll("\\dK \\d{4}-\\d*-\\d*[^<]*</font>", "</font>");
		fs = fs.replaceAll("&copy;2008 Baidu", "");
		return fs;
	}

}
