/**
 * QCM2GraphGenerator.java in dataCorrob
 * Copyright (C) by INRIA-Saclay, Alban Galland, 13 nov. 2008, All rights reserved
 */
package fr.inria.gemo.dataCorrob.graphGenerator.realDataGraphGenerator;

import java.util.Iterator;
import java.util.Vector;

import fr.inria.gemo.dataCorrob.arguments.Arguments;

/**
 * This class implements 
 */
public class QCM2GraphGenerator extends RealDataGraphGenerator {

	/**
	 * This function is the standard constructor of the class.
	 * @param args
	 */
	public QCM2GraphGenerator(Arguments args) {
		super(args);
	}

	/* (non-Javadoc)
	 * @see fr.inria.gemo.dataCorrob.graphGenerator.realDataGraphGenerator.RealDataGraphGenerator#getFile()
	 */
	@Override
	public String getFile() {
		return "data/qcm2.xml";
	}

	/* (non-Javadoc)
	 * @see fr.inria.gemo.dataCorrob.graphGenerator.realDataGraphGenerator.RealDataGraphGenerator#getSourceNames()
	 */
	@Override
	public Iterator<String> getSourceNames() {
		Vector <String> names = new Vector <String> ();
		for(int i=0;i<86;i++) {
			names.add("e"+i);
		}
		return names.iterator();
	}

}
