/**
 * RealDataGraphGenerator.java in dataCorrob
 * Copyright (C) by INRIA-Saclay, Alban Galland, 12 nov. 2008, All rights reserved
 */
package fr.inria.gemo.dataCorrob.graphGenerator.realDataGraphGenerator;

import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Iterator;

import fr.inria.gemo.dataCorrob.arguments.Arguments;
import fr.inria.gemo.dataCorrob.data.Link;
import fr.inria.gemo.dataCorrob.data.RealFact;
import fr.inria.gemo.dataCorrob.data.RealSource;
import fr.inria.gemo.dataCorrob.dataManager.GraphManager;
import fr.inria.gemo.dataCorrob.graphGenerator.GraphGenerator;

/**
 * This class implements 
 */
public abstract class RealDataGraphGenerator extends GraphGenerator {

	public abstract String getFile();
	
	public abstract Iterator <String> getSourceNames();
	
	protected RealDataGraphGenerator(Arguments args) {
		super(args);
	}
	
	protected int linkSource(GraphManager gm, Enumeration <RealFact> facts, Hashtable <String,RealFact> knownfacts, RealSource s,int id) {
		while(facts.hasMoreElements()) {
			RealFact f = facts.nextElement();
			String key = f.getAnswer()+f.getQueryId();
			if (knownfacts.containsKey(key)) {
				f = knownfacts.get(key);
			} else {
				f.setId(id);
				id++;
				gm.addFact(f);
				knownfacts.put(key, f);
			}
			gm.addChildToNode(f.getId(), s.getId(), 1);
			gm.addChildToNode(s.getId(), f.getId(), 1);
		}
		return id;
	}

	/* (non-Javadoc)
	 * @see fr.inria.gemo.dataCorrob.graphGenerator.GraphGenerator#constructGraph()
	 */
	@Override
	protected void constructGraph(GraphManager gm) {
		Hashtable <String,RealFact> knownfacts = new Hashtable <String,RealFact> ();
		int id=0;
		RealDataReader ser = new RealDataReader(this.getFile());
		Iterator <String> sourceNames = this.getSourceNames();
		while(sourceNames.hasNext()) {
			String name = sourceNames.next();
			RealSource s = new RealSource(id,name);
			id++;
			gm.addSource(s);
			Enumeration <RealFact> facts = ser.readFile(name).elements();
			id = this.linkSource(gm, facts, knownfacts, s, id);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see fr.inria.gemo.dataCorrob.graphGenerator.GraphGenerator#setPError(fr.inria.gemo.dataCorrob.dataManager.GraphManager)
	 */
	@Override
	protected void setStats(GraphManager gm) {
		Iterator <Integer> sources = gm.getSources();
		while(sources.hasNext()) {
			double nbErrors=0;
			double nbPosFacts=0;
			double nbNegFacts=0;
			int s = sources.next();
			Iterator <Link> ls = gm.getLinks(s);
			while(ls.hasNext()) {
				Link l=ls.next();
				int f = l.getTo();
				boolean isTrue = gm.getIsTrue(f);
				if((l.isPos() && !isTrue) || (l.isNeg() && isTrue)) {
					nbErrors++;
				}
				if(isTrue) {
					nbPosFacts ++;
				} else {
					nbNegFacts ++;
				}
			}
			double nTot = gm.getNbChildren(s);
			if(nTot != 0) {
				gm.setPError(s, nbErrors/nTot);
			} else {
				gm.setPError(s, 0.5);
			}
			if(gm.getNbPosFacts()!=0) {
				gm.setPForgPos(s, 1-(nbPosFacts/gm.getNbPosFacts()));
			} else {
				gm.setPForgPos(s, 0.5);
			}
			if(gm.getNbNegFacts()!=0) {
				gm.setPForgNeg(s, 1-(nbNegFacts/gm.getNbNegFacts()));
			} else {
				gm.setPForgNeg(s, 0.5);
			}
		}
		Iterator <Integer> facts = gm.getFacts();
		while(facts.hasNext()) {
			double nbErrors=0;
			int f = facts.next();
			boolean isTrue = gm.getIsTrue(f);
			Iterator <Link> ls = gm.getLinks(f);
			while(ls.hasNext()) {
				Link l=ls.next();
				if((l.isPos() && !isTrue) || (l.isNeg() && isTrue)) {
					nbErrors++;
				}
			}
			double nTot = gm.getNbChildren(f);
			if(nTot != 0) {
				gm.setPError(f, nbErrors/nTot);
			} else {
				gm.setPError(f, 0.5);
			}
			if(gm.getNbSources()!=0) {
				gm.setPForgPos(f, 1.0-(gm.getNbChildren(f)/((double) gm.getNbSources())));
				gm.setPForgNeg(f, 1.0-(gm.getNbChildren(f)/((double) gm.getNbSources())));
			} else {
				gm.setPForgPos(f, 0.5);
				gm.setPForgNeg(f, 0.5);
			}
		}
	}
}
