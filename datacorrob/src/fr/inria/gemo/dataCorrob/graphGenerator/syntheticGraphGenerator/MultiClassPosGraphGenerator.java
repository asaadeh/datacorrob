/**
 * MultiClassGraphGenerator.java in dataCorrob (C) 30 juil. 08 by agalland
 */
package fr.inria.gemo.dataCorrob.graphGenerator.syntheticGraphGenerator;

import fr.inria.gemo.dataCorrob.arguments.Arguments;

/**
 * The class implements a generator of graph with nine classes without negative
 * links
 * @author agalland (Alban Galland) for Inria Saclay
 */
public class MultiClassPosGraphGenerator extends MultiClassPosNegGraphGenerator {
	/**
	 * This function is the standard constructor of the class.
	 * @param args the arguments of the program
	 */
	public MultiClassPosGraphGenerator(Arguments args) {
		super(args);
		this.args.FactPhi.pNeg = 1;
		this.args.FactMinPhi.pNeg = 1;
		this.args.FactMaxPhi.pNeg = 1;
		this.args.SourcePhi.pNeg = 1;
		this.args.SourceMinPhi.pNeg = 1;
		this.args.SourceMaxPhi.pNeg = 1;
		this.args.FactPhi.dpNeg = 0;
		this.args.FactMinPhi.dpNeg = 0;
		this.args.FactMaxPhi.dpNeg = 0;
		this.args.SourcePhi.dpNeg = 0;
		this.args.SourceMinPhi.dpNeg = 0;
		this.args.SourceMaxPhi.dpNeg = 0;
	}
}
