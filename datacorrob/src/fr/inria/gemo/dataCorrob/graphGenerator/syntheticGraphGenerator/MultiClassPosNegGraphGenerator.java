/**
 * MultiClassPosNegGraphGenerator.java in dataCorrob (C) 31 juil. 08 by agalland
 */
package fr.inria.gemo.dataCorrob.graphGenerator.syntheticGraphGenerator;

import fr.inria.gemo.dataCorrob.arguments.Arguments;
import fr.inria.gemo.dataCorrob.arguments.EpsilonArguments;
import fr.inria.gemo.dataCorrob.arguments.PhiArguments;
import fr.inria.gemo.dataCorrob.data.SyntheticFact;
import fr.inria.gemo.dataCorrob.data.SyntheticSource;
import fr.inria.gemo.dataCorrob.dataManager.GraphManager;

/**
 * This class implements a generator of graph with nine class and positive and
 * negative links
 * @author agalland (Alban Galland) for Inria Saclay
 */
public class MultiClassPosNegGraphGenerator extends PosNegGraphGenerator {
	/**
	 * This function is the standard constructor of the class.
	 * @param args the arguments of the program
	 */
	public MultiClassPosNegGraphGenerator(Arguments args) {
		super(args);
	}

	/*
	 * (non-Javadoc)
	 * @see fr.inria.gemo.dataCorrob.graphGenerator.PosNegGraphGenerator#generateFact(int,
	 *      fr.inria.gemo.dataCorrob.dataManager.DataManager)
	 */
	@Override
	protected void generateFact(int id, GraphManager gm) {
		EpsilonArguments prec = null;
		PhiArguments forg = null;
		double sPrecMin = this.args.FactMinEpsilon.size;
		double sPrec = this.args.FactEpsilon.size;
		double sPrecMax = this.args.FactMaxEpsilon.size;
		double sForgMin = this.args.FactMinPhi.size;
		double sForg = this.args.FactPhi.size;
		double d = this.r.nextDouble();
		if (d < sForgMin * sPrecMin) {
			prec = this.args.FactMinEpsilon;
			forg = this.args.FactMinPhi;
		} else if (d < (sForgMin + sForg) * sPrecMin) {
			prec = this.args.FactMinEpsilon;
			forg = this.args.FactPhi;
		} else if (d < sPrecMin) {
			prec = this.args.FactMinEpsilon;
			forg = this.args.FactMaxPhi;
		} else if (d < sPrecMin + sForgMin * sPrec) {
			prec = this.args.FactEpsilon;
			forg = this.args.FactMinPhi;
		} else if (d < sPrecMin + (sForgMin + sForg) * sPrec) {
			prec = this.args.FactEpsilon;
			forg = this.args.FactPhi;
		} else if (d < sPrecMin + sPrec) {
			prec = this.args.FactEpsilon;
			forg = this.args.FactMaxPhi;
		} else if (d < sPrecMin + sPrec + sForgMin * sPrecMax) {
			prec = this.args.FactMaxEpsilon;
			forg = this.args.FactMinPhi;
		} else if (d < sPrecMin + sPrec + (sForgMin + sForg) * sPrecMax) {
			prec = this.args.FactMaxEpsilon;
			forg = this.args.FactPhi;
		} else {
			prec = this.args.FactMaxEpsilon;
			forg = this.args.FactMaxPhi;
		}
		boolean value = true;
		if (this.r.nextDouble() > this.args.rateTrue) {
			value = false;
		}
		gm.addFact(new SyntheticFact(id, value, this.computeProb(prec.p, prec.dp), this
				.computeProb(forg.pPos, forg.dpPos), this.computeProb(
				forg.pNeg, forg.dpNeg),"q"+id));
	}

	/*
	 * (non-Javadoc)
	 * @see fr.inria.gemo.dataCorrob.graphGenerator.PosNegGraphGenerator#generateSource(int,
	 *      fr.inria.gemo.dataCorrob.dataManager.DataManager)
	 */
	@Override
	protected void generateSource(int id, GraphManager gm) {
		EpsilonArguments prec = null;
		PhiArguments forg = null;
		double sPrecMin = this.args.SourceMinEpsilon.size;
		double sPrec = this.args.SourceEpsilon.size;
		double sPrecMax = this.args.SourceMaxEpsilon.size;
		double sForgMin = this.args.SourceMinPhi.size;
		double sForg = this.args.SourcePhi.size;
		double d = this.r.nextDouble();
		if (d < sForgMin * sPrecMin) {
			prec = this.args.SourceMinEpsilon;
			forg = this.args.SourceMinPhi;
		} else if (d < (sForgMin + sForg) * sPrecMin) {
			prec = this.args.SourceMinEpsilon;
			forg = this.args.SourcePhi;
		} else if (d < sPrecMin) {
			prec = this.args.SourceMinEpsilon;
			forg = this.args.SourceMaxPhi;
		} else if (d < sPrecMin + sForgMin * sPrec) {
			prec = this.args.SourceEpsilon;
			forg = this.args.SourceMinPhi;
		} else if (d < sPrecMin + (sForgMin + sForg) * sPrec) {
			prec = this.args.SourceEpsilon;
			forg = this.args.SourcePhi;
		} else if (d < sPrecMin + sPrec) {
			prec = this.args.SourceEpsilon;
			forg = this.args.SourceMaxPhi;
		} else if (d < sPrecMin + sPrec + sForgMin * sPrecMax) {
			prec = this.args.SourceMaxEpsilon;
			forg = this.args.SourceMinPhi;
		} else if (d < sPrecMin + sPrec + (sForgMin + sForg) * sPrecMax) {
			prec = this.args.SourceMaxEpsilon;
			forg = this.args.SourcePhi;
		} else {
			prec = this.args.SourceMaxEpsilon;
			forg = this.args.SourceMaxPhi;
		}
		gm
				.addSource(new SyntheticSource(id, this.computeProb(prec.p, prec.dp), this
						.computeProb(forg.pPos, forg.dpPos), this.computeProb(
						forg.pNeg, forg.dpNeg)));
	}
}
