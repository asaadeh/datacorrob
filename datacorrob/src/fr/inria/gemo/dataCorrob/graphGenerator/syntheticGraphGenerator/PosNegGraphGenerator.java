/**
 * PosNegGraphGenerator.java in dataCorrob (C) 30 juil. 08 by agalland
 */
package fr.inria.gemo.dataCorrob.graphGenerator.syntheticGraphGenerator;

import java.util.Iterator;
import java.util.Random;

import fr.inria.gemo.dataCorrob.arguments.Arguments;
import fr.inria.gemo.dataCorrob.data.SyntheticFact;
import fr.inria.gemo.dataCorrob.data.SyntheticSource;
import fr.inria.gemo.dataCorrob.dataManager.GraphManager;
import fr.inria.gemo.dataCorrob.graphGenerator.GraphGenerator;

/**
 * This class implements a generator of graph with one class and positive and
 * negative links
 * @author agalland (Alban Galland) for Inria Saclay
 */
public class PosNegGraphGenerator extends GraphGenerator {
	/**
	 * The internal way to generates random number
	 */
	protected Random r;

	/**
	 * This function is the standard constructor of the class.
	 * @param args the arguments of the program
	 */
	public PosNegGraphGenerator(Arguments args) {
		super(args);
		this.r = new Random();
	}

	/*
	 * (non-Javadoc)
	 * @see fr.inria.gemo.dataCorrob.graphGenerator.GraphGenerator#constructGraph()
	 */
	@Override
	public void constructGraph(GraphManager gm) {
		for (int i = 0; i < this.args.nbSources; i++) {
			this.generateSource(i, gm);
		}
		for (int i = this.args.nbSources; i < this.args.nbFacts
				+ this.args.nbSources; i++) {
			this.generateFact(i, gm);
			this.linkFact(i, gm);
		}
	}

	/**
	 * This function generates a new fact
	 * @param id the id of the fact
	 * @param dm the data manager
	 */
	protected void generateFact(int id, GraphManager gm) {
		boolean value = true;
		if (this.r.nextDouble() > this.args.rateTrue) {
			value = false;
		}
		gm.addFact(new SyntheticFact(id, value, this.computeProb(this.args.FactEpsilon.p,
				this.args.FactEpsilon.dp), this.computeProb(
				this.args.FactPhi.pPos, this.args.FactPhi.dpPos), this
				.computeProb(this.args.FactPhi.pNeg,
						this.args.FactPhi.dpNeg),"q"+id));
	}

	/**
	 * This function generates a new source
	 * @param id the id of the source
	 * @param dm the data manager
	 */
	protected void generateSource(int id, GraphManager gm) {
		gm.addSource(new SyntheticSource(id, this.computeProb(this.args.SourceEpsilon.p,
				this.args.SourceEpsilon.dp), this.computeProb(
				this.args.SourcePhi.pPos, this.args.SourcePhi.dpPos),
				this.computeProb(this.args.SourcePhi.pNeg,
						this.args.SourcePhi.dpNeg)));
	}

	/**
	 * This function generates link to a fact
	 * @param id the id of the fact
	 * @param dm the data manager
	 */
	private void linkFact(int id, GraphManager gm) {
		for (int i = 0; i < this.args.nbSources; i++) {
			double probPhi;
			double value;
			if (this.r.nextDouble() < (gm.getEpsilon(id)
					* gm.getEpsilon(i))) {
				if (gm.getIsTrue(id)) {
					probPhi = gm.getPhiNeg(id)
							* gm.getPhiNeg(i);
					value = -1;
				} else {
					probPhi = gm.getPhiPos(id)
							* gm.getPhiPos(i);
					value = 1;
				}
			} else {
				if (gm.getIsTrue(id)) {
					probPhi = gm.getPhiPos(id)
							* gm.getPhiPos(i);
					value = 1;
				} else {
					probPhi = gm.getPhiNeg(id)
							* gm.getPhiNeg(i);
					value = -1;
				}
			}
			if (this.r.nextDouble() > probPhi) {
				gm.addChildToNode(id, i, value);
				gm.addChildToNode(i, id, value);
			}
		}
	}

	/**
	 * This function computes randomly a value for the probability, given an
	 * average and a delta
	 * @param mu the average of the sub-distribution
	 * @param dP the delta of the sub-distribution
	 * @return the value of the probability
	 */
	protected double computeProb(double mu, double dP) {
		Double d = mu;
		Double p = this.r.nextDouble();
		if (p > 0.5) {
			d = Math.min(1, mu + dP / 2.0 - Math.sqrt(dP * dP / 2.0 * (1 - p)));
		} else {
			d = Math.max(0, Math.sqrt(dP * dP / 2.0 * p) + mu - dP / 2.0);
		}
		return d;
	}

	@Override
	protected void setStats(GraphManager gm) {
		double sumEpsilonFacts=0;
		Iterator <Integer> facts = gm.getFacts();
		while(facts.hasNext()) {
			sumEpsilonFacts += gm.getEpsilon(facts.next());
		}
		double avgEpsilonFacts = sumEpsilonFacts/gm.getNbFacts();
		double sumEpsilonSources=0;
		Iterator <Integer> sources = gm.getSources();
		while(sources.hasNext()) {
			sumEpsilonSources += gm.getEpsilon(sources.next());
		}
		double avgEpsilonSources = sumEpsilonSources/gm.getNbSources();
		facts = gm.getFacts();
		while(facts.hasNext()) {
			int f = facts.next();
			gm.setPError(f, gm.getEpsilon(f)*avgEpsilonSources);
		}
		sources = gm.getSources();
		while(sources.hasNext()) {
			int s = sources.next();
			gm.setPError(s, gm.getEpsilon(s)*avgEpsilonFacts);
		}
	}
}
